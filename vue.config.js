/**
 * 配置参考:
 * https://cli.vuejs.org/zh/config/
 */
const url = 'http://pigx-gateway:9999'
// const url = 'http://gateway.haohanwork.com'
// const mock_url = 'http://haohan-api.com/mock/70';

module.exports = {
  lintOnSave: true,
  productionSourceMap: false,
  chainWebpack: config => {
    // 忽略的打包文件
    config.externals({
      // 'vue': 'Vue',
      // 'vue-router': 'VueRouter',
      // 'vuex': 'Vuex',
      'axios': 'axios',
    })
    const entry = config.entry('app')
    entry
      .add('babel-polyfill')
      .end()
    entry
      .add('classlist-polyfill')
      .end()
  },


  // 配置转发代理
  devServer: {
    disableHostCheck: true,
    port: 8081,
    hotOnly:false,

    proxy: {
      '/pds': {
        target: url,
        // target: 'http://api-pds.com:9888',
        // target: 'http://localhost:8080',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/pds': '/haohanshop'
          // '^/pds': '/pds-manage'
        }
      },
      '/auth': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/auth': '/auth'
        }
      },
      '/admin': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/admin': '/admin'
        }
      },
      '/code': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/code': '/code'
        }
      },
      '/gen': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/gen': '/gen'
        }
      },
      '/actuator': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/actuator': '/actuator'
        }
      },
      '/monitor': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/monitor': '/monitor'
        }
      },
      '/mp': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/mp': '/mp'
        }
      },
      '/daemon': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/daemon': '/daemon'
        }
      },
      '/job': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/job': '/job'
        }
      },
      '/tx': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/tx': '/tx'
        }
      },
      '/act': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/act': '/act'
        }
      },
      '/demo': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/demo': '/demo'
        }
      },
      '/purchase': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/purchase': '/purchase'
        }
      },
      '/supply': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/supply': '/supply'
        }
      },

      '/product': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/product': '/product'
        }
      }, '/market': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/market': '/market'
        }
      }, '/saleb': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/saleb': '/saleb'
        }
      },
      '/salec': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/salec': '/salec'
        }
      },
      '/oms': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/oms': '/oms'
        }
      },
      '/wms': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/wms': '/wms'
        }
      },
      '/tms': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/tms': '/tms'
        }
      },
      '/bi': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/bi': '/bi'
        }
      },
      '/retail': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/retail': '/retail'
        }
      },
      '/aftersales': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/aftersales': '/aftersales'
        }
      },
      '/manage': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/manage': '/manage'
        }
      },
      '/common': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/common': '/common'
        }
      },
      '/goods': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/goods': '/goods'
        }
      },
      '/pay': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/pay': '/pay'
        }
      },
      '/iot': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/iot': '/iot'
        }

      },
      '/opc': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/opc': '/opc'
        }
      },
      '/crm': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/crm': 'crm'
        }
      },
      '/bill': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/bill': 'bill'
        }
      },
        '/msg': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/msg': 'msg'
        }
      },
      '/tenant': {
        target: url,
        ws: true,
        pathRewrite: {
          '^/tenant': ''
        }
      },
    }
  },
  configureWebpack: {
    externals: {
      'AMap': 'AMap' // 高德地图配置
    }
  }
}

