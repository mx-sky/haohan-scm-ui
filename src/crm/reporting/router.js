import Layout from '@/page/index/'

const reportingRouter = {
    path: '/reporting',
    // redirect: '/customer/customerList',
    component: Layout,
    meta: { title: '汇报管理' },
    children: [{
        path: 'template',
        component: () => import('./template.vue'),
        name: '模板页面',
        meta: { title: '模板页面' }
    }, {
        path: 'daySummary',
        component: () => import('./daySummary.vue'),
        name: '日报汇总',
        meta: { title: '日报汇总' }
    }, {
        path: 'dayInquiries',
        component: () => import('./dayInquiries.vue'),
        name: '日报查询',
        meta: { title: '日报查询' }
    }, {
        path: 'dayInquiriesAdd',
        component: () => import('./dayInquiriesAdd.vue'),
        name: '新增日报',
        meta: { title: '新增日报' }
    }, {
        path: 'dayInquiriesEdit',
        component: () => import('./dayInquiriesEdit.vue'),
        name: '编辑日报',
        meta: { title: '编辑日报' }
    }, {
        path: 'dayInquiriesDetail',
        component: () => import('./dayInquiriesDetail.vue'),
        name: '日报查询详情',
        meta: { title: '日报查询详情' },
    }, {
        path: 'salesDay',
        component: () => import('./salesDay/list.vue'),
        name: '销量上报',
        meta: { title: '销量上报' }
    }, {
        path: 'resale',
        component: () => import('./salesDay/resale.vue'),
        name: '零售销量查询',
        meta: { title: '零售销量查询' }
    }, {
        path: 'addSalesDay',
        component: () => import('./salesDay/addSalesDay.vue'),
        name: '新增销量上报',
        meta: { title: '新增销量上报' }
    }, {
        path: 'editSalesDay',
        component: () => import('./salesDay/editSalesDay.vue'),
        name: '编辑销量上报',
        meta: { title: '编辑销量上报' }
    }, {
        path: 'salesDetail',
        component: () => import('./salesDay/detail.vue'),
        name: '销量上报详情',
        meta: { title: '销量上报详情' }
    }
    , {
        path: 'salesDayReview',
        component: () => import('./salesDay/review.vue'),
        name: '销量上报审核',
        meta: { title: '销量上报审核' }
    }, {
        path: 'inventoryIndate',
        component: () => import('./inventory/indate.vue'),
        name: '近效期查询',
        meta: { title: '近效期查询' }
    }
      , {
        path: 'inventory',
        component: () => import('./inventory/list.vue'),
        name: '库存上报',
        meta: { title: '库存上报' }
    }, {
        path: 'addInventory',
        component: () => import('./inventory/addInventory.vue'),
        name: '新增库存上报',
        meta: { title: '新增库存上报' }
    }, {
        path: 'editInventory',
        component: () => import('./inventory/editInventory.vue'),
        name: '编辑库存上报',
        meta: { title: '编辑库存上报' }
    }, {
        path: 'inventoryDetail',
        component: () => import('./inventory/detail.vue'),
        name: '库存上报详情',
        meta: { title: '库存上报详情' }
    }, {
        path: 'inventoryReview',
        component: () => import('./inventory/inventoryReview.vue'),
        name: '库存上报审核',
        meta: { title: '库存上报审核' }
    }
    , {
        path: 'competition',
        component: () => import('./competition/list.vue'),
        name: '竞品上报',
        meta: { title: '竞品上报' }
    }, {
        path: 'addCompetition',
        component: () => import('./competition/add.vue'),
        name: '新增竞品',
        meta: { title: '新增竞品' }
    }, {
        path: 'editCompetition',
        component: () => import('./competition/edit.vue'),
        name: '编辑竞品',
        meta: { title: '编辑竞品' }
    }, {
        path: 'competitionDetail',
        component: () => import('./competition/detail.vue'),
        name: '竞品详情',
        meta: { title: '竞品详情' }
    }, {
        path: 'competitionReview',
        component: () => import('./competition/competitionReview.vue'),
        name: '竞品上报审核',
        meta: { title: '竞品上报审核' }
    }]
}

export default reportingRouter;
