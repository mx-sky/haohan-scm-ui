import Layout from '@/page/index/'

const customerRouter = {
  path: '/customer',
  component: Layout,
  meta: {title: '客户数据库'},
  children: [
      {
        path: 'customerNews',
        component: () => import('./customer_list/index.vue'),
        name: '客户信息',
        meta: {title: '客户信息'}
      }, {
        path: 'addCustomer',
        component: () => import('./customer_list/addCustomer.vue'),
        name: '新增客户',
        meta: {title: '新增客户'}
      }, {
        path: 'editCustomer',
        component: () => import('./customer_list/editCustomer.vue'),
        name: '编辑客户',
        meta: {title: '编辑客户'}
      },  {
        path: 'customerDetail',
        component: () => import('./customer_list/customerDetail.vue'),
        name: '客户详情',
        meta: {title: '客户详情'}
      }, {
        path: 'customerMap',
        component: () => import('./customerMap.vue'),
        name: '客户分布地图',
        meta: {title: '客户分布地图'}
      },{
        path: 'customerLog',
        component: () => import('./customerLog.vue'),
        name: '客户变更日志',
        meta: {title: '客户变更日志'}
      },{
        path: 'customerTerritory',
        component: () => import('./customerTerritory.vue'),
        name: '销售区域管理',
        meta: {title: '销售区域管理'}
      },{
        path: 'customerCom',
        component: () => import('./customerCom.vue'),
        name: '客户组织结构',
        meta: {title: '客户组织结构'}
      },{
        path: 'customerVisit',
        component: () => import('./customervisit/index.vue'),
        name: '客户拜访记录',
        meta: {title: '客户拜访记录'}
      },{
        path: 'addVisit',
        component: () => import('./customervisit/addVisit.vue'),
        name: '新增客户拜访记录',
        meta: {title: '新增客户拜访记录'}
      },{
        path: 'editVisit',
        component: () => import('./customervisit/editVisit.vue'),
        name: '编辑客户拜访记录',
        meta: {title: '编辑客户拜访记录'}
      },{
        path: 'detailVisit',
        component: () => import('./customervisit/detailVisit.vue'),
        name: '客户拜访记录详情',
        meta: {title: '客户拜访记录详情'}
      },{
        path: 'customerInventory',
        component: () => import('./customerInventory.vue'),
        name: '客户库存',
        meta: {title: '客户库存'}
      },{
        path: 'commemorationDay',
        component: () => import('./tionDay/commemorationDay.vue'),
        name: '客户纪念日',
        meta: {title: '客户纪念日'}
      },{
        path: 'addDay',
        component: () => import('./tionDay/addDay.vue'),
        name: '新增客户纪念日',
        meta: {title: '新增客户纪念日'}
      },{
        path: 'editDay',
        component: () => import('./tionDay/editDay.vue'),
        name: '编辑客户纪念日',
        meta: {title: '编辑客户纪念日'}
      },{
        path: 'detailDay',
        component: () => import('./tionDay/detailDay.vue'),
        name: '客户纪念日详情',
        meta: {title: '客户纪念日详情'}
      },
  ]
}

export default customerRouter;
