import Layout from '@/page/index/'

const orderConRouter = {
    path: '/orderCon',
    // redirect: '/orderCenter/supply_match',
    component: Layout,
    meta: { title: '订单管理' },
    children:[
        {
          path: 'salesOrder',
          component: () => import('./orderpayrecord/index.vue'),
          name: '销售单',
          meta: {title: '销售单'}
        },{
          path: 'salesOrderDetail',
          component: () => import('./orderpayrecord/detail.vue'),
          name: '销售单详情',
          meta: {title: '销售单详情'}
        },{
          path: 'editOrder',
          component: () => import('./orderpayrecord/editOrder.vue'),
          name: '编辑销售单',
          meta: {title: '编辑销售单'}
        },{
            path: 'Place_order',
            component: () => import('./customerOrder/index.vue'),
            name: '代客下单',
            meta: {title: '代客下单'}
        },
        {
            path: 'salesOrderReview',
            component: () => import('./orderpayrecord/review.vue'),
            name: '销售单审核',
            meta: {title: '销售单审核'}
        },
        {
          path: 'slip_order',
          component: () => import('./return_order/index.vue'),
          name: '退货订单',
          meta: {title: '退货订单'}
        }, {
          path: 'slip_detail',
          component: () => import('./return_order/detail'),
          name: '退货订单详情',
          meta: {title: '退货订单详情'}
        },
        {
          path: 'slip_edit',
          component: () => import('./return_order/edit'),
          name: '编辑退货订单',
          meta: {title: '编辑退货订单'}
        },
        {
          path: 'slip_handleAdd',
          component: () => import('./return_order/handleAdd'),
          name: '新增退货单',
          meta: {title: '新增退货单'}
        },
        {
          path: 'slip_cheack',
          component: () => import('./return_order/cheack'),
          name: '审核退货单',
          meta: {title: '审核退货单'}
        },
        {
          path: 'slip_Warehousing',
          component: () => import('./return_order/Warehousing'),
          name: '退货单入库',
          meta: {title: '退货单入库'}
        },
        {
          path: 'slip_takeOver',
          component: () => import('./return_order/takeOver'),
          name: '退货单确认收货',
          meta: {title: '退货单确认收货'}
        }
    ]
}

export default orderConRouter;

