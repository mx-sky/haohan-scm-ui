import Layout from '@/page/index/'

// 仓库管理
const breakageRouter = {
  path: '/breakage',
  redirect: '/breakage/breakage_list ',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'breakage_list',
      component: () => import('./../breakage'),
      name: '盘点'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详情'
    },
    {
      path: 'redact',
      component: () => import('./redact'),
      name: '编辑'
    },
    {
      path: 'increased',
      component: () => import('./increased'),
      name: '新增'
    }
  ]
}

export default breakageRouter;
