import Layout from '@/page/index/'

// 仓库管理
const exitWarehouseRouter = {
  path: '/exit_warehouse',
  redirect: '/exit_warehouse/exit_warehouse_list',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'exit_warehouse_list',
      component: () => import('./../exit_warehouse'),
      name: '出库管理'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '出库单详情'
    },
    {
      path: 'redact',
      component: () => import('./redact'),
      name: '编辑出库单'
    },
    {
      path: 'increased',
      component: () => import('./increased'),
      name: '新增出库单'
    }
  ]
}

export default exitWarehouseRouter;
