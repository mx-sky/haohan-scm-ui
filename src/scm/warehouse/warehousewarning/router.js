import Layout from '@/page/index/'

// 仓库管理
const warehousewarningRouter = {
  path: '/warehousewarning',
  redirect: '/warehousewarning/warehousewarning_list ',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'warehousewarning_list',
      component: () => import('./../warehousewarning'),
      name: '盘点'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详情'
    },
    // {
    //   path: 'detail',
    //   component: () => import('./detail'),
    //   name: '详情'
    // }
  ]
}

export default warehousewarningRouter;
