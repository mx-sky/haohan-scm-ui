import Layout from '@/page/index/'

// 仓库管理
const enterWarehouseRouter = {
  path: '/enter_warehouse',
  redirect: '/enter_warehouse/enter_warehouse_list',
  component: Layout,
  meta: {
    title: '仓库'
  },
  children: [{
      path: 'enter_warehouse_list',
      component: () => import('./../enter_warehouse'),
      name: '入库管理'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '入库单详情'
    },
    {
      path: 'redact',
      component: () => import('./redact'),
      name: '入库编辑'
    },
    {
      path: 'increased',
      component: () => import('./increased'),
      name: '新增入库单'
    }
  ]
}

export default enterWarehouseRouter;
