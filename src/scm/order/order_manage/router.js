import Layout from '@/page/index/'

// 订单管理
const orderManageRouter = {
    path: '/order_manage',
    redirect: '/order_manage/order_manage_list',
    component: Layout,
    meta: {title: '订单'},
    children: [
        {
            path: 'order_manage_list',
            component: () => import('./../order_manage'),
            name: '订单管理'
        },
        {
          path: 'edit',
          component: () => import('./edit'),
          name: '订单编辑'
        },
        {
            path: 'detail',
            component: () => import('./detail'),
            name: '订单详情'
        },
        {
            path: 'valetOrder',
            component: () => import('../../../pds/order/customerOrder'),
            name: '代客下单'
        }
    ]
}

export default orderManageRouter;
