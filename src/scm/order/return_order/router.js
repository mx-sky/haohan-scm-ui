import Layout from '@/page/index/'

// 订单退货
const sendBackRouter = {
    path: '/return_order',
    redirect: '/order/returnOrderList',
    component: Layout,
    meta: {title: '订单退货'},
    children: [
        {
            path: 'returnOrderList',
            component: () => import('./../return_order'),
            name: '订单退货列表'
        },
       {
            path: 'detail',
            component: () => import('./../return_order/detail'),
            name: '退货单详情'
        },
        {
            path: 'edit',
            component: () => import('./../return_order/edit'),
            name: '编辑退货单'
        },
         {
            path: 'handleAdd',
            component: () => import('./../return_order/handleAdd'),
            name: '新增退货单'
        },
        {
          path: 'cheack',
          component: () => import('./../return_order/cheack'),
          name: '审核退货单'
        },
        {
          path: 'Warehousing',
          component: () => import('./../return_order/Warehousing'),
          name: '退货单入库'
        },
        {
          path: 'takeOver',
          component: () => import('./../return_order/takeOver'),
          name: '退货单确认收货'
        }
    ]
}

export default sendBackRouter;
