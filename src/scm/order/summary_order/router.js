import Layout from '@/page/index/'

// 订单汇总
const summaryOrderRouter = {
    path: '/summary_order',
    redirect: '/summary_order/summary',
    component: Layout,
    meta: {title: '订单'},
    children: [
        {
            path: 'summary',
            component: () => import('./../summary_order'),
            name: '订单汇总'
        },
        {
          path: 'needpurchasedetail',
          component: () => import('./detail/needPurchaseDetail'),
          name: '需采购汇总详情'
        },
        {
          path: 'stockupdetail',
          component: () => import('./detail/stockUpDetail'),
          name: '可备货汇总详情'
        },


    ]
}

export default summaryOrderRouter;
