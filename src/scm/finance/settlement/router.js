import Layout from '@/page/index/'

// 结算记录
const orderManageRouter = {
  path: '/settlement',
  redirect: '/settlement/receivable_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
      {
          path: 'receivable_list',
          component: () => import('./receivable/index.vue'),
          name: '应收结算记录列表',
          meta: {title: '应收结算记录列表'}
      },
      {
          path: 'payable_list',
          component: () => import('./payable/index.vue'),
          name: '应付结算记录列表',
          meta: {title: '应付结算记录列表'}
      },
      {
          path: 'settlement_detail',
          component: () => import('./edit.vue'),
          name: '结算信息',
          meta: {title: '结算信息'}
      }
  ]
}

export default orderManageRouter;
