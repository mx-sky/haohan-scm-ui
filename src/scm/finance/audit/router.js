import Layout from '@/page/index/'

// 订单管理
const orderManageRouter = {
  path: '/audit',
  redirect: '/audit/audit_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'audit_list',
      component: () => import('./../audit'),
      name: '应收账单'
    },
    {
      path: 'handleAdd',
      component: () => import('./handleAdd'),
      name: '新增'
    },
    {
      path: 'detail',
      component: () => import('./detail'),
      name: '详细'
    },

    /* {
       path: 'valetOrder',
       component: () => import('../../../scm/order/customerOrder/components/goodsItem'),
       name: '代客下单'
     }*/
  ]
}

export default orderManageRouter;
