import Layout from '@/page/index/'

// 应收账单
const orderManageRouter = {
  path: '/receivable',
  redirect: '/receivable/receivable_list',
  component: Layout,
  meta: {title: '财务管理'},
  children: [
    {
      path: 'receivable_list',
      component: () => import('./../receivable'),
      name: '应收账单'
    },
    // {
    //   path: 'detail',
    //   component: () => import('./detail'),
    //   name: '详细'
    // }
  ]
}

export default orderManageRouter;
