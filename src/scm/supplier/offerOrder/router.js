import Layout from '@/page/index/'

// 供应商报价单
const offerOrderRouter = {
    path: '/offer_order',
    redirect: '/offer_order/offer_order_list',
    component: Layout,
    meta: {title: '供应商报价单'},
    children: [
        {
            path:'offer_order_list',
            component:() => import('./index'),
            name:'供应商报价单',
            meta:{title:'供应商报价单列表'}
        },
    ]
}

export default offerOrderRouter;
