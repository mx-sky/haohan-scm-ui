import Layout from '@/page/index/'

// 供应商详情
const supplierDetailRouter = {
    path: '/supplierorder',
    redirect: '/supplierorder/supplierorderlist',
    component: Layout,
    meta: {title: '供应商详情'},
    children: [
        {
            path:'supplierorderlist',
            component:() => import('./../supplierorder'),
            name:'供应商详情',
            meta:{title:'供应商订单列表'}
        },
        {
            path: 'showDetail',
            component: () => import('./../supplierorder/detail'),
            name: '供应商订单详情',
            meta: {title: '供应商订单详情'}
        }
    ]
}

export default supplierDetailRouter;
