import Layout from '@/page/index/'

// 采购管理
const marketQuotationRouter = {
    path: '/market_quotation',
    redirect: '/market_quotation/market_quotation_list',
    component: Layout,
    meta: {title: '采购管理'},
    children: [
        {
            path: 'market_quotation_list',
            component: () => import('./../market_quotation'),
            name: '市场行情'
        },
    ]
}

export default marketQuotationRouter;
