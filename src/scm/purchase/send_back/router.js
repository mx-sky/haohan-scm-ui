import Layout from '@/page/index/'

// 采购退货
const sendBackRouter = {
    path: '/purchase',
    redirect: '/purchase/sendBackList',
    component: Layout,
    meta: {title: '采购退货'},
    children: [
        {
            path: 'sendBackList',
            component: () => import('./../send_back'),
            name: '采购退货列表'
        },
        {
            path: 'addSendBack',
            component: () => import('./../send_back/add'),
            name: '新增采购退货'
        },
        {
          path: 'cheack',
          component: () => import('./../send_back/cheack'),
          name: '审核采购退货'
        },
        {
          path: 'edit',
          component: () => import('./../send_back/edit'),
          name: '采购退货编辑'
        },
        {
            path: 'sendBackDetail',
            component: () => import('./../send_back/detail'),
            name: '采购退货详情'
        }
    ]
}

export default sendBackRouter;
