import Layout from '@/page/index/'

// 采购单
const purchaseTaskRouter = {
    path: '/purchase_task',
    redirect: '/purchase_task/purchase_task_list',
    component: Layout,
    meta: {title: '采购单'},
    children: [
        {
            path: 'purchase_task_list',
            component: () => import('./../purchase_task'),
            name: '采购单列表'
        },
    ]
}

export default purchaseTaskRouter;
