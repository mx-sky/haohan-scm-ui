import Layout from '@/page/index/'

// 采购员工管理
const purchaseEmployeeRouter = {
    path: '/purchase_employee',
    redirect: '/purchase_employee/list',
    component: Layout,
    meta: {title: '采购员工管理'},
    children: [
        {
            path:'list',
            component:() => import('./../purchase_employee'),
            name:'采购员工列表',
            meta:{title:'采购员工列表'}
        },
        {
            path: 'add',
            component: () => import('./../purchase_employee/add'),
            name: '添加采购员工',
            meta: {title: '添加采购员工'}
        },
        {
            path: 'edit',
            component: () => import('./../purchase_employee/add'),
            name: '编辑采购员工信息',
            meta: {title:"编辑采购员工信息"}
        },
      {
            path: 'detail',
            component: () => import('./../purchase_employee/detail'),
            name: '采购员工详情',
            meta: {title:"采购员工详情信息"}
        },
    ]
}

export default purchaseEmployeeRouter;
