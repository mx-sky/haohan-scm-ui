import Layout from '@/page/index/'

// 采购商管理
const purchaseDataRouter = {
    path: '/clientmanage',
    redirect: '/clientmanage/clientmanagelist',
    component: Layout,
    meta: {title: '采购商管理'},
    children: [
        {
            path:'clientmanagelist',
            component:() => import('./../clientmanage'),
            name:'采购商列表',
            meta:{title:'采购商列表'}
        },
        {
            path: 'add',
            component: () => import('./../clientmanage/add'),
            name: '添加采购商',
            meta: {title: '添加采购商'}
        },
        {
            path: 'edit',
            component: () => import('./../clientmanage/add'),
            name: '编辑采购商信息',
            meta: {title:"编辑采购商信息"}
        },
      {
            path: 'detail',
            component: () => import('./../clientmanage/detail'),
            name: '采购商详情',
            meta: {title:"采购商详情信息"}
        },
    ]
}

export default purchaseDataRouter;
