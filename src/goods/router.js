import Layout from '@/page/index/'
const goodsRouter = {
  path: '/goods',
  redirect: '/goods/goods_list',
  component: Layout,
  meta: { title: '商品管理' },
  children: [
    {
      path: 'addsupplygoods',
      component: () => import('./goods_list/add.vue'),
      name: '新增商品',
      meta: { title: '新增商品' }
    },
    {
      path: 'editsupplygoods',
      component: () => import('./goods_list/edit.vue'),
      name: '编辑商品',
      meta: { title: '编辑商品' }
    },
    {
      path: 'detailsupplygoods',
      component: () => import('./goods_list/detail_goods.vue'),
      name: '商品详情',
      meta: { title: '商品详情' }
    },
    {
      path: "editCategory",
      component: () => import('./category/add_category'),
      name: "编辑分类",
      meta: {title: '编辑分类'}
    },
    {
      path: 'addCategory',
      component: () => import('./category/add_category'),
      name: "新增分类",
      meta: {title: '新增分类'}
    },
  ]
}
export default goodsRouter;
