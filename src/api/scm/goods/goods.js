/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'
// 修改分类是否显示
export function changeCategoryStatus(obj) {
  return request({
    url: '/goods/goodscategory/changeCategoryStatus',
    method: 'post',
    data: obj,
    headers: {"Content-Type":"application/json"}
  })
}
// 修改分类上下架
export function offGoodsByCate(obj) {
  return request({
    url: '/goods/goodscategory/offGoodsByCate',
    method: 'post',
    data: obj
  })
}
