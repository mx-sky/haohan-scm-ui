/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/saleb/buyorder/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'put',
        data: obj
    })
}
export function getOrderDetail(pmId,buyId){
    return request({
        url:'/saleb/api/saleb/buyOrder/queryBuyOrderDetailList/'+pmId+"/"+buyId,
        method:'get',
    })
}
export function createBuyOrderDetailList(obj){
    return request({
        url:'/saleb/api/saleb/buyOrder/createBuyOrderDetailByGoods',
        method:'post',
        data:obj,
        headers: {"Content-Type":"application/json"}
    })
}


export function confirmReceive(obj){
    return request({
        url:'/saleb/api/saleb/buyOrder/confirmReceive',
        method:'post',
        params:obj,
    })
}

