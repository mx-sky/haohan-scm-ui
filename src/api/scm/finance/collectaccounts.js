/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

// 应收账单

export function fetchList(query) {
    return request({
        url: '/opc/buyerpayment/fetchPage',
        method: 'get',
        params: query
    })
}
//应收账单详情
export function getDetail(params) {
  return request({
    url: '/opc/api/opc/receivable/queryPaymentDetail',
    method: 'get',
    params: params,
  })
}
// 创建应收账单
export function createReceivable(obj) {
    return request({
        url: '/opc/api/opc/receivable/createReceivable',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}

// 批量创建 订单应收账单
export function createReceivableBatch(obj) {
    return request({
        url: '/opc/api/opc/receivable/createReceivable/batch',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}


// 准备结算
export function readySettlement(obj) {
    return request({
        url: '/opc/api/opc/settlement/readySettlement',
        method: 'post',
        data: obj,
        headers: {"Content-Type": "application/json"}
    })
}

// 查询是否可审核
export function checkReceivable(obj) {
    return request({
        url: '/opc/api/opc/receivable/checkReceivable',
        method: 'post',
        params: obj
    })
}

// 查询是否可审核
export function reviewPayment(obj) {
    return request({
        url: '/opc/api/opc/receivable/reviewPayment',
        method: 'post',
        params: obj
    })
}

