/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

/**
 * 查询入库单列表
 * @param query
 */
export function fetchList(query) {
    return request({
        url: '/wms/enterwarehouse/page',
        method: 'get',
        params: query
    })
}
export function list(query) {
    return request({
        url: '/saleb/buyorder/page',
        method: 'get',
        params: query
    })
}

/**
 * 查询入库单明细（包含货品信息）根据入库单号
 * @param query
 */
export function queryList(query) {
    return request({
        url: '/wms/api/wms/enterWarehouse/queryEnterOrderDetail',
        method: 'get',
      params: query,
    })
}


//新增入库单
export function createBuyOrderDetailList(obj){
    return request({
        url:'/wms/api/wms/enterWarehouse/createEnterWarehouseDetail',
        method:'post',
        data:obj,
        headers: {"Content-Type":"application/json"}
    })
}

//编辑入库单
export function updateEnterWarehouseDetail(data){
  return request({
    url:'/wms/api/wms/enterWarehouse/editEnterWarehouse',
    method:'post',
    data:data,
    headers: {"Content-Type":"application/json"}
  })
}

export function getObj(id) {
    return request({
        url: '/wms/enterwarehouse/' + id,
        method: 'get'
    })
}

