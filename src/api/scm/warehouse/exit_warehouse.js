/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/wms/exitwarehouse/page',
        method: 'get',
        params: query
    })
}
/**
 * 查询出库单明细（包含货品信息）根据出库单号
 * @param query
 */
export function queryList(query) {
  return request({
    url: '/wms/api/wms/scmExitWarehouse/queryExitWarehouseDetail',
    method: 'post',
    data: query,
    headers: {"Content-Type":"application/json"}
  })
}
//新增出库单
export function addExitWarehouse(obj){
  return request({
    url:'/wms/api/wms/scmExitWarehouse/addExitWarehouse',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}
//编辑出库单
export function redactExitWarehouse(obj){
  return request({
    url:'/wms/api/wms/scmExitWarehouse/redactExitWarehouse',
    method:'post',
    data:obj,
    headers: {"Content-Type":"application/json"}
  })
}
//获取商品列表
export function goodsList(query) {
  return request({
    url: '/goods/goodsmodel/queryGoodsModelDTOList',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/purchase/purchasetask/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/purchase/purchasetask',
        method: 'put',
        data: obj
    })
}
