/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/aftersales/returnorder/page',
    method: 'get',
    params: query
  })
}

//新增退货单
export function addSend(params) {
  return request({
    url: '/aftersales/api/returnOrder/addReturnOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//编辑退货单
export function editSend(params) {
  return request({
    url: '/aftersales/api/returnOrder/editReturnOrder',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//退货单详情
export function getSendDeatil(params) {
  return request({
    url: '/aftersales/api/returnOrder/queryReturnDetail',
    method: 'get',
    params: params,
  })
}
//退款
export function refund(obj) {
  return request({
    url: '/saleb/api/saleb/payment/createReceivable',
    method: 'post',
    data: obj
  })
}

export function addObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/purchase/purchasetask/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/purchase/purchasetask',
    method: 'put',
    data: obj
  })
}

