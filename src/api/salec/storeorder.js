/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/salec/storeorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/salec/storeorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/salec/storeorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/salec/storeorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/salec/storeorder',
    method: 'put',
    data: obj
  })
}

export function transBuyOrder(params) {
    return request({
        url: '/salec/api/salec/order/trans',
        method: 'post',
        data: params,
    })
}
