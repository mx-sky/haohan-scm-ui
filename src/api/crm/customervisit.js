/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/customervisit/page',
    method: 'get',
    params: query
  })
}

//新增客户
export function addVisit(params) {
  return request({
    url: '/crm/api/crm/customerVisit/visitEdit',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

// 新增客户
export function addCustomerVisit(params) {
  return request({
    url: '/crm/api/crm/customerVisit/visitPlan',
    method: 'post',
    params: params,
    headers:{"Content-type":"application/json"},
  })
}

export function getDetail(query) {
  return request({
    url: '/crm/api/crm/customerVisit/visitInfo',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/customervisit',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/customervisit/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/customervisit/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/customervisit',
    method: 'put',
    data: obj
  })
}

// 拜会分析
export function getMonthAnalysis(params) {
  return request({
    url: '/crm/api/crm/customerVisit/monthAnalysis',
    method: 'get',
    params: params,
  });
}

// 客户展示数据
export function getSummaryAnalysis(params) {
  return request({
    url: '/crm/api/crm/analysis/customer/summaryInfo',
    method: 'get',
    params: params,
  });
}