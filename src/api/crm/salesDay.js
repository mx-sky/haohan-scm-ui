/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/salesReport/queryList',
    method: 'get',
    params: query
  })
}

//新增销量上报
export function addReport(params) {
  return request({
    url: '/crm/api/crm/salesReport/addReport',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

//编辑销量上报
export function modifyReport(params) {
  return request({
    url: '/crm/api/crm/salesReport/modifyReport',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}
//删除销量上报
export function deleteReport(query) {
  return request({
    url: '/crm/api/crm/salesReport/deleteReport',
    method: 'get',
    params: query
  })
}
// 确认销量上报
export function reportReview(params) {
  return request({
    url: '/crm/api/crm/salesReport/review',
    method: 'post',
    params: params
  })
}

//销售上报详情
export function getDetail(params) {
  return request({
    url: '/crm/api/crm/salesReport/queryInfo',
    method: 'get',
    params: params,
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'put',
    data: obj
  })
}
