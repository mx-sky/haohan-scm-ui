/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/customerlevel/page',
    method: 'get',
    params: query
  })
}
//树
export function areaTree() {
  return request({
    url: '/crm/api/crm/customer/customerTree',
    method: 'get',
  })
}
//编辑层级
export function editLevel(params) {
  return request({
    url: '/crm/api/crm/customer/editLevel',
    method: 'post',
    params: params
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/customerlevel',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/customerlevel/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/customerlevel/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/customerlevel',
    method: 'put',
    data: obj
  })
}

// 批量创建客户商家
export function createMerchantBatch(obj) {
  return request({
    url: '/crm/api/crm/customer/merchantBatch',
    method: 'post',
    data: obj
  })
}

