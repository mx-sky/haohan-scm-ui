/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/salesorderdetail/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/salesorderdetail',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/salesorderdetail/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/salesorderdetail/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/salesorderdetail',
    method: 'put',
    data: obj
  })
}
