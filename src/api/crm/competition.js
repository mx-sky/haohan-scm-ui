import request from '@/router/axios'

export function getCompetitionList(query) {
  return request({
    url: '/crm/api/crm/competitionReport/queryList',
    method: 'get',
    params: query
  })
}

// 详情
export function getCompetitionDetail(query) {
    return request({
        url: '/crm/api/crm/competitionReport/queryInfo',
        method: 'get',
        params: query
    })
}

export function addCompetition(data) {
    return request({
        url: '/crm/api/crm/competitionReport/addReport',
        method: 'post',
        data: data
    })
}

export function updateCompetition(data) {
    return request({
        url: '/crm/api/crm/competitionReport/modifyReport',
        method: 'post',
        data: data
    })
}

export function removeCompetition(query) {
    return request({
        url: '/crm/api/crm/competitionReport/deleteReport',
        method: 'get',
        params: query
    })
}

export function sureCompetition(params) {
    return request({
        url: '/crm/api/crm/competitionReport/review',
        method: 'post',
        params: params,
    })
}