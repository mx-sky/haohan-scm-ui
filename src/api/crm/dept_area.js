
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/api/crm/relation/area/deptQueryPage',
    method: 'get',
    params: query
  })
}

// 查看一个部门的区域
export function fetchDeptArea(query) {
    let params = {
        deptId: query
    }
    return request({
        url: '/crm/api/crm/relation/area/deptQueryOne',
        method: 'get',
        params: params
    })
}

// 编辑部门的区域
export function editDeptArea(params) {
  return request({
    url: '/crm/api/crm/relation/area/deptEdit',
    method: 'post',
    data: params
  })
}



