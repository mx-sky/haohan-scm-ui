/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/salescontract/page',
    method: 'get',
    params: query
  })
}

export function contractEdit(query) {
  return request({
    url: '/crm/api/crm/contract/contractEdit',
    method: 'post',
    data: query
  })
}

export function getDetail(query) {
  return request({
    url: '/crm/api/crm/contract/contractInfo',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/salescontract/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/salescontract',
    method: 'put',
    data: obj
  })
}
