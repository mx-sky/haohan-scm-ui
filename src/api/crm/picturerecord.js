/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/picturerecord/page',
    method: 'get',
    params: query
  })
}
//现场图片详情
export function fetchPicture(params) {
  return request({
    url: '/crm/api/crm/customer/fetchPicture',
    method: 'get',
    params: params,
  })
}

//新增现场图片
export function editPicture(params) {
  return request({
    url: '/crm/api/crm/customer/editPicture',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}
export function addObj(obj) {
  return request({
    url: '/crm/picturerecord',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/picturerecord/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/picturerecord/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/picturerecord',
    method: 'put',
    data: obj
  })
}
