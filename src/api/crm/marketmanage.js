/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/crm/marketmanage/page',
        method: 'get',
        params: query
    })
}


//新增
export function marketEdit(params) {
    return request({
        url: '/crm/api/crm/marketManage/marketEdit',
        method: 'post',
        data: params,
        headers: {"Content-Type": "application/json"}
    })
}

//详情
export function getDetail(query) {
    return request({
        url: '/crm/api/crm/marketManage/marketInfo',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/crm/marketmanage',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/crm/marketmanage/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/crm/marketmanage/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/crm/marketmanage',
        method: 'put',
        data: obj
    })
}

// 迁移市场下客户
export function moveMarket(params) {
    return request({
        url: '/crm/api/crm/marketManage/move',
        method: 'post',
        params: params
    })
}
