/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/crm/customer/page',
    method: 'get',
    params: query
  })
}
//新增客户
export function addCustomer(params) {
  return request({
    url: '/crm/api/crm/customer/edit',
    method: 'post',
    data: params,
    headers:{"Content-Type":"application/json"}
  })
}

export function addObj(obj) {
  return request({
    url: '/crm/customer',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/crm/customer/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/crm/customer/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/crm/customer',
    method: 'put',
    data: obj
  })
}
