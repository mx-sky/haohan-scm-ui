/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/wms/enterwarehouse/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/wms/enterwarehouse',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/wms/enterwarehouse/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/wms/enterwarehouse/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/wms/enterwarehouse',
    method: 'put',
    data: obj
  })
}
