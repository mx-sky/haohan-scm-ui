/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/wms/shelf/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/wms/shelf',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/wms/shelf/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/wms/shelf/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/wms/shelf',
    method: 'put',
    data: obj
  })
}
