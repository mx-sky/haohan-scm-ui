/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/opc/buyerpayment/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/opc/buyerpayment',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/opc/buyerpayment/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/opc/buyerpayment/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/opc/buyerpayment',
    method: 'put',
    data: obj
  })
}
//查询待结算应收帐单
export function countAccount(query) {
    return request({
        url: '/opc/buyerpayment/countByBuyerPaymentReq',
        method: 'post',
        data: query,
    })
}
