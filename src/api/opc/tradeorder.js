/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/opc/tradeorder/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/opc/tradeorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/opc/tradeorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/opc/tradeorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/opc/tradeorder',
    method: 'put',
    data: obj
  })
}
