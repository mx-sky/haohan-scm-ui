/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/manage/photogroupmanage/page',
    method: 'get',
    params: query
  })
}

export function getDetail(query) {
  return request({
    url: '/manage/api/manage/photo/fetchGroup',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/manage/photogroupmanage',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/manage/photogroupmanage/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/manage/photogroupmanage/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/manage/photogroupmanage',
    method: 'put',
    data: obj
  })
}
