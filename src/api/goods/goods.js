/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/goods/goods/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/goods/goods',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/goods/goods/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/goods/goods/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/goods/goods',
    method: 'put',
    data: obj
  })
}

export function getSliderImage(goodsSn){
    return request({
        url: '/goods/goods/getSliderImage/'+goodsSn,
        method: 'get'
    })
}

export function uploadSliderImages(obj) {
    return request({
        url: '/goods/goods/uploadSliderImages',
        method: 'post',
        data:obj,
        headers: {"Content-Type":"application/json"}
    })
}

// 商品列表
export function getGoodsList(params = {}) {
  return request({
    url: '/goods/api/goods/manage/fetchInfoPage',
    method: 'get',
    params: params,
  })
}

// 规格列表
export function getGoodsModelList(params = {}) {
  return request({
    url: '/goods/api/goods/manage/fetchModelList',
    method: 'get',
    params: params,
  })
}
// 获取编辑页信息
export function getEditInfo(params) {
  return request({
    url: "/goods/api/goods/manage/fetchInfo",
    method: 'get',
    params: params
  })
}
//商品修改
export function changeEditInfo(params) {
  return request({
    url: "/goods/api/goods/manage/info",
    method: 'put',
    data: params
  })
}
//特殊商品修改
export function editSpecialGoods(params) {
  return request({
    url: "/goods/api/goods/special/info",
    method: "PUT",
    data: params
  })
}
//特殊商品新增
export function addSpecialGoods(params) {
  return request({
    url: "/goods/api/goods/special/info",
    method: 'POST',
    data: params
  })
}
