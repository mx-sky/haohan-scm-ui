/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/goods/goodsmodel/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/goods/goodsmodel',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/goods/goodsmodel/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/goods/goodsmodel/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/goods/goodsmodel',
    method: 'put',
    data: obj
  })
}

export function getModelList(query = {}) {
  return request({
    url: '/goods/api/goods/manage/fetchModelList',
    method: 'get',
    params: query
  })
}
