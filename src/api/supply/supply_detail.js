import request from '@/router/axios'
export function fetchSupplyInfo(params) {
    return request({
        url: '/supply/api/supply/supplier/info',
        method: 'get',
        params: params
    })
}
