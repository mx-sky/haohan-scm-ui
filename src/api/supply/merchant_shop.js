import request from '@/router/axios'

export function merchantInfo(supplierId) {
    return request({
        url:'/supply/api/supply/supplier/merchantInfo',
        method: 'get',
        params: {supplierId: supplierId}
    })
}
export function delObj(id) {
    return request({
      url: '/supply/supplier/' + id,
      method: 'delete'
    })
  }

  export function fetchList(query) {
    return request({
      url: '/supply/supplier/page',
      method: 'get',
      params: query
    })
  }