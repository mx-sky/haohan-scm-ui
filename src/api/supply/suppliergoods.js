/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/supply/suppliergoods/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/supply/suppliergoods',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/supply/suppliergoods/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/supply/suppliergoods/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/supply/suppliergoods',
    method: 'put',
    data: obj
  })
}
//查询供应商的商品列表
export function getGoodsList(params={}) {
  return request({
    url: "/supply/api/supply/goods/page",
    method: "get",
    params: params
  })
}
//供应商品新增到平台售卖
export function putPlatformSales(params) {
  return request({
    url: "/supply/api/supply/goods/platformSales",
    method: "post",
    params: params
  })
}
//取消已在平台售卖的供应商品
export function cancelPlatformSales(params) {
  return request({
    url: "/supply/api/supply/goods/platformSales",
    method: "DELETE",
    params: params
  })
}
//商品上下架修改
export function isMarketTable(params) {
  return request({
    url: '/goods/api/goods/manage/marketable',
    method: 'put',
    params: params
  })
}
//商品删除
export function delGoods(id) {
  return request({
    url: '/goods/api/goods/manage/' + id,
    method: "DELETE"
  })
}
//商品新增
export function addGoods(params) {
  return request({
    url: '/goods/api/goods/manage/info',
    method: 'post',
    data: params
  })
}
//商品排序(权重)修改
export function updateSort(params) {
  return request({
    url: '/goods/api/goods/manage/sort',
    method: 'put',
    params: params
  })
}
// 商品库存修改
export function updateStorage(params) {
  return request({
    url: '/goods/api/goods/manage/storage',
    method: 'put',
    params: params
  })
}
//商品规格库存修改
export function updateModelStorage(params) {
  return request({
    url: '/goods/api/goods/manage/model/storage',
    method: 'put',
    params: params
  })
}

