/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/supply/offerorder/page',
    method: 'get',
    params: query
  })
}
//创建退货单
export function addReturnOrderByOfferOrder(query) {
  return request({
    url: '/aftersales/api/returnOrder/addReturnOrderByOfferOrder',
    method: 'post',
    data: query,
    headers: {"Content-Type":"application/json"}
  })
}

export function addObj(obj) {
  return request({
    url: '/supply/offerorder',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/supply/offerorder/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/supply/offerorder/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
    return request({
        url: '/supply/offerorder',
        method: 'put',
        data: obj
    })
}


