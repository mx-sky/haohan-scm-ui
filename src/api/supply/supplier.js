/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/supply/supplier/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/supply/supplier',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/supply/supplier/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/supply/supplier/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/supply/supplier',
    method: 'put',
    data: obj
  })
}
// 已有供应商的商家列表
export function getmerchantList(params = {}) {
  return request({
    method: 'get',
    url: '/supply/api/supply/supplier/merchantList'
  })
}
// 获取供应商详情
export function editSupplyDetails(params) {
  return request({
    url: '/supply/api/supply/supplier/info',
    method: 'get',
    params: params
  })
}

// 获取商家列表
export function getMerchantObj(query) {
  return request({
    url: '/manage/api/manage/merchant/page',
    method: 'get',
    params: query
  })
}

