import request from '@/router/axios'

export function addObj(obj) {
        return request({
          url: '/saleb/buyer',
          method: 'post',
          data: obj
        })
      }

// 获取商家列表
export function getMerchantObj(query) {
        return request({
          url: '/manage/api/manage/merchant/page',
          method: 'get',
          params: query
        })
      }
