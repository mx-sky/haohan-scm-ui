import request from '@/router/axios'
// 采购商家详情
export function buyerDetails(params) {
        return request({
                url: '/saleb/api/saleb/buyer/info',
                method: 'get',
                params: params
        })
}