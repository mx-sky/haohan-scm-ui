import request from '@/router/axios'

export function putObj(obj) {
        return request({
                url: '/saleb/buyer',
                method: 'put',
                data: obj
        })
}

// 获取供应商详情
export function editSupplyDetails(params) {
        return request({
                url: '/saleb/api/saleb/buyer/info',
                method: 'get',
                params: params
        })
}
// 获取商家列表
export function getMerchantObj(query) {
        return request({
                url: '/manage/api/manage/merchant/page',
                method: 'get',
                params: query
        })
}