/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

// 查询商家列表
export function fetchList(query) {
  return request({
    url: '/saleb/buyer/page',
    method: 'get',
    params: query
  })
}

// 新增采购商家
export function addObj(obj) {
  return request({
    url: '/saleb/buyer',
    method: 'post',
    data: obj
  })
}
// 采购商家详情
export function getObj(id) {
  return request({
    url: '/saleb/buyer/' + id,
    method: 'get'
  })
}
// 删除采购商家
export function delObj(id) {
  return request({
    url: '/saleb/buyer/' + id,
    method: 'delete'
  })
}
// 修改采购商家
export function putObj(obj) {
  return request({
    url: '/saleb/buyer',
    method: 'put',
    data: obj
  })
}


export function queryUPassport(obj) {
    return request({
        url: '/saleb/api/saleb/buyer/queryUPassport',
        method: 'get',
        params: obj
    })
}

export function queryUser(obj) {
    return request({
        url: '/saleb/api/saleb/buyer/queryUser',
        method: 'get',
        params: obj
    })
}
//查询采购商列表
export function getBuyerList(prams) {
  return request({
    url: '/saleb/buyer/page',
    method: 'get',
    params: prams
  })
}

