/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
    return request({
        url: '/product/productinfo/page',
        method: 'get',
        params: query
    })
}

export function addObj(obj) {
    return request({
        url: '/product/productinfo',
        method: 'post',
        data: obj
    })
}

export function getObj(id) {
    return request({
        url: '/product/productinfo/' + id,
        method: 'get'
    })
}

export function delObj(id) {
    return request({
        url: '/product/productinfo/' + id,
        method: 'delete'
    })
}

export function putObj(obj) {
    return request({
        url: '/product/productinfo',
        method: 'put',
        data: obj
    })
}

export function createSortingOrder(obj) {
    return request({
        url: '/product/api/product/productInfo/createSortingOrder',
        method: 'post',
        data: obj,
        headers: {"Content-Type":"application/json"}
    })
}
