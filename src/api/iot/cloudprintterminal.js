/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/iot/cloudprintterminal/page',
    method: 'get',
    params: query
  })
}
//新增打印机
export function addCloudPrinter(params) {
  return request({
    url: '/iot/api/iot/cloudPrintTerminal/addCloudPrinter',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//编辑打印机
export function editCloudPrintTerminal(params) {
  return request({
    url: '/iot/api/iot/cloudPrintTerminal/editCloudPrintTerminal',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}
//删除打印机
export function deleteById(params) {
  return request({
    url: '/iot/api/iot/cloudPrintTerminal/deleteById',
    method: 'post',
    data: params,
    headers: {"Content-Type":"application/json"}
  })
}

export function addObj(obj) {
  return request({
    url: '/iot/cloudprintterminal',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/iot/cloudprintterminal/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/iot/cloudprintterminal/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/iot/cloudprintterminal',
    method: 'put',
    data: obj
  })
}
