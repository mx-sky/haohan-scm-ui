/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/tms/deliveryflow/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/tms/deliveryflow',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/tms/deliveryflow/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/tms/deliveryflow/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/tms/deliveryflow',
    method: 'put',
    data: obj
  })
}
