/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/aftersales/customerrevaluate/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/aftersales/customerrevaluate',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/aftersales/customerrevaluate/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/aftersales/customerrevaluate/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/aftersales/customerrevaluate',
    method: 'put',
    data: obj
  })
}
