import request from '@/router/axios'

/**
 * 分页列表
 */
export function fetchList(params) {
    return request({
        url: '/bill/api/bill/settlement/page',
        method: 'get',
        params: params
    })
}

/**
 * 结算单详情
 */
export function fetchInfo(params) {
    return request({
        url: '/bill/api/bill/settlement/queryInfo',
        method: 'get',
        params: params
    })
}

/**
 * 结算单准备结算
 */
export function readySettlement(params) {
    return request({
        url: '/bill/api/bill/settlement/ready',
        method: 'post',
        params: params
    })
}

/**
 * 结算单完成收款结算
 */
export function normalFinish(params) {
    return request({
        url: '/bill/api/bill/settlement/finish',
        method: 'post',
        data: params,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 * 多账单合并结算单
 */
export function summarySettlement(params) {
    return request({
        url: '/bill/api/bill/settlement/summary/create',
        method: 'post',
        data: params,
        headers: {"Content-Type": "application/json"}
    })
}

/**
 * 结算撤销
 */
export function revokeSettlement(params) {
    return request({
        url: '/bill/api/bill/settlement/revoke',
        method: 'post',
        params: params
    })
}