/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/purchase/purchaseemployee/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/purchase/purchaseemployee',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/purchase/purchaseemployee/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/purchase/purchaseemployee/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/purchase/purchaseemployee',
    method: 'put',
    data: obj
  })
}
