import Layout from '@/page/index/'

const photoRouter = {
    path: '/photo',
    redirect: '/photo/galleryList',
    component: Layout,
    meta: {title: '图片管理'},
    children: [
        {
            path: 'galleryList',
            component: () => import('./photo_gallery/index.vue'),
            name: '图片列表',
            meta: {title: '图片列表'}
        },
        {
            path: 'groupList',
            component: () => import('./photo_group/index.vue'),
            name: '图片组列表',
            meta: {title: '图片组列表'}
        },
        {
            path: 'groupDetail',
            component: () => import('./photo_group/detail.vue'),
            name: '图片组详情',
            meta: {title: '图片组详情'}
        },
    ]
};

export default photoRouter;
