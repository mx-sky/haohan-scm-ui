import 'babel-polyfill'
import 'classlist-polyfill'
import Vue from 'vue'
import axios from './router/axios'
import VueAxios from 'vue-axios'
import App from './App'
import './permission' // 权限
import './error' // 日志
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import AVUE from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
import router from './router/router'
import store from './store'
import { loadStyle } from './util/util'
import * as urls from '@/config/env'
import {iconfontUrl, iconfontVersion} from '@/config/env'
import * as filters from './filters' // 全局filter
import './styles/common.scss'
// import avueFormDesign from 'avue-form-design'
import basicContainer from './components/basic-container/main'
// 插件 json 展示
// import vueJsonTreeView from 'vue-json-tree-view'

import {validatenull} from '@/util/validate'
import {filterForm} from '@/util/util'
import {getStore} from "@/util/store"
//PDS 系统合并
import utils from "./util/pds/utils.js";
import constant from '../public/scm/const.js';
import dic from "./util/pds/dictionary.js";
import components from "./components/pds/common/common.js";
// 地图坐标选择器
import AvueMap from 'avue-plugin-map';

import i18n from './lang'


let $utils = new utils();

Vue.prototype.$utils = $utils;
Vue.prototype.$dic = dic;
Vue.prototype.$constant = constant;
Vue.prototype.$http = axios;

Vue.use(components);

Vue.prototype.validatenull = validatenull
Vue.prototype.filterForm = filterForm
Vue.prototype.getStore = getStore


Vue.use(AvueMap);


// Vue.use(avueFormDesign);
// Vue.use(vueJsonTreeView)

Vue.use(VueAxios, axios)


Vue.use(ElementUI, {
  size: 'medium',
  menuType: 'text',
  i18n: (key, value) => i18n.t(key, value)
})


Vue.use(AVUE, {
  size: 'medium',
  menuType: 'text',
  i18n: (key, value) => i18n.t(key, value),
  ali: {
    region: 'oss-cn-beijing',
    endpoint: 'oss-cn-beijing.aliyuncs.com',
    accessKeyId: 'LTAIJqCc4O3gDa6r',
    accessKeySecret: 'KFTTdy0aBhCt1k0hVyryTbDrRGfvFG',
    bucket: 'ysg-aliyun',
  },
})

//设置input为number  只能输入整数
Vue.directive('enterNumber', {
  inserted: function (el) {
    el.addEventListener("keypress",function(e){
      e.target.value = (e.target.value.match(/^\d*(\.?\d{0,2})/g)[0]) || null
    });
  }
})

// 注册全局容器
Vue.component('basicContainer', basicContainer)

// 加载相关url地址
Object.keys(urls).forEach(key => {
  Vue.prototype[key] = urls[key]
})

// 加载过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 动态加载阿里云字体库
iconfontVersion.forEach(ele => {
  loadStyle(iconfontUrl.replace('$key', ele))
})

Vue.config.productionTip = false


new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
