import Layout from '@/page/index/'
const supplyRouter = {
  path: '/supply',
  redirect: '/supply/supplier',
  component: Layout,
  meta: { title: '供应管理' },
  children: [
    {
      path: 'supplier',
      component: () => import('./supplier/index.vue'),
      name: '供应商信息',
      meta: { title: '供应商信息' }
    },
    {
      path: 'supplierdetail',
      component: () => import('./supplier/detail.vue'),
      name: '查看供应商详情',
      meta: { title: '查看供应商详情' }
    },
    {
      path: 'supplieredit',
      component: () => import('./supplier/edit.vue'),
      name: '编辑供应商',
      meta: { title: '编辑供应商' }
    },
    {
      path: 'supplieradd',
      component: () => import('./supplier/add.vue'),
      name: '新增供应商信息',
      meta: { title: '新增供应商信息' }
    },
    {
      path: 'supplyorder',
      component: () => import('./supplyorder/index.vue'),
      name: '供应商订单',
      meta: { title: '供应商订单' }
    },
    {
      path: 'editsupplyorder',
      component: () => import('./supplyorder/edit.vue'),
      name: '供应订单编辑',
      meta: { title: '供应订单编辑' }
    },
    {
      path: 'addsupplyorder',
      component: () => import('./supplyorder/add.vue'),
      name: '新增供应订单',
      meta: { title: '新增供应订单' }
    }, {
      path: 'supplygoods',
      component: () => import('./supplygoods/index.vue'),
      name: '供应商品',
      meta: { title: '供应商品' }
    }, {
      path: 'addsupplygoods',
      component: () => import('./supplygoods/add.vue'),
      name: '新增供应商品',
      meta: { title: '新增供应商品' }
    }, {
      path: 'editsupplygoods',
      component: () => import('./supplygoods/edit.vue'),
      name: '编辑供应商品',
      meta: { title: '编辑供应商品' }
    },{
      path: 'detailsupplygoods',
      component: () => import('./supplygoods/detail_goods'),
      name: '供应商品详情',
      meta: { title: '供应商品详情' }
    },
    {
      path: 'goodsputawaysupply',
      component: () => import('./goodsputaway/index.vue'),
      name: '平台商品供应',
      meta: { title: '平台商品供应' }
    },
    {
      path: 'purchasedeatilssupplygoods',
      component: () => import('./order_list/purchase_details.vue'),
      name: '平台采购供应详情',
      meta: { title: '平台采购供应详情' }
    },
    {
      path: 'modelGoodsDetail',
      component: () => import('./goodsputaway/model_goods_detail'),
      name: '平台商品详情',
      meta: {title: '平台商品详情'}
    },
    {
      path: 'goodscategory',
      component: () => import('./goodscategory/index'),
      name: '供应商品分类',
      meta: { title: '供应商品分类' }
    },
    {
      path: 'addCategory',
      component: () => import('./goodscategory/add_category'),
      name: "新增分类",
      meta: {title: '新增分类'}
    },
    {
      path: "editCategory",
      component: () => import('./goodscategory/add_category'),
      name: "编辑分类",
      meta: {title: '编辑分类'}
    },
    {
      path: "supplyOrderDetail",
      component: () => import('./order_list/supply_order_detail'),
      name: "供应订单详情",
      meta: {title: '供应订单详情'}
    },
    {
      path: "purchaseOrderOrderDetail",
      component: () => import('./order_list/purchase_order'),
      name: "平台采购供应",
      meta: {title: '平台采购供应'}
    },
    {
      path: "supplyOrder",
      component: () => import('./order_list/index'),
      name: "供应商订单",
      meta: {title: '供应商订单'}
    }

  ]
}
export default supplyRouter
