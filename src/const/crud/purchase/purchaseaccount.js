/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        //  {
        //   label: '账户拥有人id',
        //   prop: 'accountOwnerId'
        // },
        {
            label: '账户拥有人名称',
            prop: 'accountOwnerName',
            search: true,
            sortable: true
        },
        {
            label: '账户余额',
            prop: 'accountBalance',
            sortable: true
        },
        {
            label: '账户状态',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_use_status',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
