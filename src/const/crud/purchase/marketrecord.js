/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        {
            label: '商品规格ID',
            prop: 'goodsModelId',
            search: true,
            sortable: true
        },
        {
            label: '商品图片',
            prop: 'goodsImg',
            type: 'upload',
            imgWidth: 100,
            imgHeight: 50,
            listType: 'picture-img'
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            search: true,
            sortable: true
        },
        {
            label: '规格名称',
            prop: 'modelName',
            search: true,
            sortable: true
        },
        {
            label: '单位',
            prop: 'unit',
            search: true,
            sortable: true
        },
        {
            label: '市场价',
            prop: 'marketPrice',
            sortable: true
        },
        {
            label: '记录时间',
            prop: 'recordTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '发起人',
            prop: 'initiatorId',
            search: true,
            sortable: true
        },
        //  {
        //   label: '供应商id',
        //   prop: 'supplierId'
        // },
        {
            label: '供应商名称',
            prop: 'supplierName',
            search: true,
            sortable: true
        },{
            label: '商品分类id',
            prop: 'goodsCategoryId',
            search: true,
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
