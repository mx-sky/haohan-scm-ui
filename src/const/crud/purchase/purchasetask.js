/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家ID',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        //  {
        //   label: '发起人',
        //   prop: 'initiatorId'
        // },
        {
            label: '发起人名称',
            prop: 'initiatorName'
        },
        //  {
        //   label: '执行人',
        //   prop: 'transactorId'
        // },
        {
            label: '执行人名称',
            prop: 'transactorName'
        },
        {
            label: '采购单编号',
            prop: 'purchaseSn',
            search: true,
            sortable: true
        },
        {
            label: '采购单明细编号',
            prop: 'purchaseDetailSn',
            search: true,
            sortable: true
        },
        {
            label: '任务执行方式',
            prop: 'taskActionType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_task_action_type',
            search: true,
            sortable: true
        },
        {
            label: '任务状态',
            prop: 'taskStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_task_status',
            search: true,
            sortable: true
        },
        {
            label: '任务内容说明',
            prop: 'content'
        },
        {
            label: '任务执行备注',
            prop: 'actionDesc'
        },
        {
            label: '任务截止时间',
            prop: 'deadlineTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '任务操作时间',
            prop: 'actionTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
