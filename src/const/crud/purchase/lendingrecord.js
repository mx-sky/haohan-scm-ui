/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        {
            label: '采购明细编号',
            prop: 'purchaseDetailSn'
            ,
            search: true,
            sortable: true
        },
        //  {
        //   label: '申请人',
        //   prop: 'applicantId'
        // },
        {
            label: '申请人名称',
            prop: 'applicantName',
            search: true,
            sortable: true
        },
        {
            label: '申请内容',
            prop: 'applicantContent'
        },
        {
            label: '申请时间',
            prop: 'applyTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        //  {
        //   label: '审核人',
        //   prop: 'auditorId'
        // },
        {
            label: '审核人名称',
            prop: 'auditorName',
            search: true,
            sortable: true
        },
        {
            label: '审核人意见',
            prop: 'auditorOpinion'
        },
        {
            label: '审核时间',
            prop: 'auditTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        //  {
        //   label: '复审人',
        //   prop: 'reviewerId'
        // },
        {
            label: '复审人名称',
            prop: 'reviewerName',
            search: true,
            sortable: true
        },
        {
            label: '复审人意见',
            prop: 'reviewerOpinion'
        },
        {
            label: '复审时间',
            prop: 'reviewTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '申请金额',
            prop: 'applyAmount'
        },
        //  {
        //   label: '收款人',
        //   prop: 'payee'
        // },
        {
            label: '收款人名称',
            prop: 'payeeName',
            search: true,
            sortable: true
        },
        {
            label: '放款金额',
            prop: 'lendingAmount',
            sortable: true
        },
        //  {
        //   label: '放款人',
        //   prop: 'lenderId'
        // },
        {
            label: '放款人名称',
            prop: 'lenderName',
            search: true,
            sortable: true
        },
        {
            label: '放款时间',
            prop: 'lendingTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '放款说明',
            prop: 'lengingContent'
        },
        {
            label: '请款状态',
            prop: 'lendingStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_lending_status',
            search: true,
            sortable: true
        },
        //  {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
