/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        //  {
        //   label: '主键',
        //   prop: 'id'
        // },
        {
            label: '平台商家id',
            prop: 'pmId',
            search: true,
            sortable: true
        },
        {
            label: '采购单编号',
            prop: 'purchaseSn',
            search: true,
            sortable: true
        },
        {
            label: '采购订单分类',
            prop: 'purchaseOrderType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_order_type',
            search: true,
            sortable: true
        },
        {
            label: '采购截止时间',
            prop: 'buyFinalTime',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search: true,
            sortable: true
        },
        {
            label: '采购状态',
            prop: 'purchaseStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_status',
            search: true,
            sortable: true
        },
        {
            label: '审核方式',
            prop: 'reviewType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_review_type',
            search: true,
            sortable: true
        },
        {
            label: '发起人',
            prop: 'initiatorId',
            search: true,
            sortable: true
        },
        {
            label: '预估总价',
            prop: 'estimateAmount'
        },
        {
            label: '实际总价',
            prop: 'dealAmount'
        },
        // {
        //   label: '创建者',
        //   prop: 'createBy'
        // },
        //  {
        //   label: '创建时间',
        //   prop: 'createDate'
        // },
        //  {
        //   label: '更新者',
        //   prop: 'updateBy'
        // },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            type: 'textarea'
        },
        //  {
        //   label: '删除标记',
        //   prop: 'delFlag'
        // },
        //  {
        //   label: '租户id',
        //   prop: 'tenantId'
        // },
    ]
}
