/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '结算相关',
            prop: 'group2',
            column: [
                {
                    type: 'datetime',
                    label: '结算时间',
                    prop: 'settlementTime',
                    format: "yyyy-MM-dd HH:mm:ss",
                    valueFormat: "yyyy-MM-dd HH:mm:ss",
                    span: 12
                },
                {
                    label: '付款方式',
                    prop: 'payType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_pay_type',
                    valueDefault: '2',
                    span: 12
                },
            ]
        },
        {
            icon: 'el-icon-info',
            label: '记录信息',
            prop: 'group3',
            column: [
                {
                    type: 'input',
                    label: '结款人名称',
                    prop: 'companyOperator',
                    span: 8
                },
                {
                    type: 'input',
                    label: '经办人名称',
                    prop: 'operatorName',
                    span: 8
                },
                {
                    type: "textarea",
                    label: "结算说明",
                    prop: "settlementDesc",
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                },
                {
                    type: 'input',
                    label: '结算凭证图片',
                    labelWidth: 120,
                    prop: 'photoList',
                    formslot: true,
                    span: 24
                }
            ]
        }
    ]
}
