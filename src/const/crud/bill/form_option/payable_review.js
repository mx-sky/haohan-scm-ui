/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '审核信息',
            prop: 'group1',
            column: [
                {
                    type: "textarea",
                    label: "审核备注",
                    prop: "remarks",
                    maxlength: 255,
                    span: 12,
                    showWordLimit: true
                }
            ]
        }
    ]
}
