/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 100,
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  viewBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '通行证id',
      prop: 'uid',
      sortable: true,
    },
    {
      label: '小程序名称',
      prop: 'appName',
    },
    {
      label: '微信昵称',
      prop: 'nickName',
      search: true,
    },
    {
      label: '头像',
      prop: 'albumUrl',
      type: 'img',
      dataType: 'string',
    }, {
      label: '性别',
      prop: 'status',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_sex'
    }, {
      label: '注册时间',
      prop: 'createTime',
    },{
      label: '更新时间',
      prop: 'updateDate',
    }
  ]
}


