export const editOption = {
        emptyBtn: false,
        detail: false,
        // menuBtn:false,
        group: [
          {
            label: '基本信息',
            prop: 'group1',
      
            column: [
              {
                label: '采购商名称',
                prop: 'buyerName',
                maxlength: 10,
                labelWidth: 120,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入采购商名称',
                  trigger: 'blur'
                }]
      
              }, {
                label: '采购商简称',
                prop: 'shortName',
                maxlength: 10,
                labelWidth: 120,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入采购商简称',
                  trigger: 'blur'
                }]
              }, {
                label: '商家',
                prop: 'merchantId',
                maxlength: 10,
                formslot: true,
              }, {
                label: '联系人',
                prop: 'contact',
                maxlength: 10,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入联系人',
                  trigger: 'blur'
                }]
              }, {
                label: '电话',
                prop: 'telephone',
                "type": "number",
                // precision:Number,
                precision: 0,
                maxlength: 11,
                minlength: 11,
                showWordLimit: true,
                rules: [{
                  required: true,
                  message: '请输入电话',
                  trigger: 'blur',
                }]
              },
              {
                label: '详细地址',
                prop: 'address',
                showWordLimit: true,
                formslot: true
      
              },
              {
                "type": "input",
                "label": "权限角色",
                "prop": "roleIds",
                showWordLimit: true,
                formslot: true
                // span: 8
              }, {
                label: '标注位置 ',
                prop: 'position',
                showWordLimit: true,
                formslot: true
                // span: 8
              },
      
              {
                label: '部门',
                prop: 'deptId',
                formslot: true,
      
              },
              {
                label: '区域',
                prop: 'area',
                formslot: true
              }, {
                label: '备注',
                prop: 'remarks',
                type: 'textarea',
                maxlength: 200,
                showWordLimit: true
              }]
          }, {
            label: '扩展信息',
            prop: 'group2',
            column: [{
              label: '采购商类型',
              prop: 'buyerType',
              type: 'radio',
              // value: '0',
              dicUrl: '/admin/dict/type/scm_buyer_type'
            }, {
              label: '启用状态',
              type: 'radio',
              prop: 'status',
              // value: '1',
              dicUrl: '/admin/dict/type/scm_use_status'
            }, {
              label: '账期',
              type: 'radio',
              prop: 'payPeriod',
              // value: '1',
              dicUrl: '/admin/dict/type/scm_pay_period'
            }, {
              label: '消息推送',
              type: 'radio',
              prop: 'needPush',
              // value: '0',
              dicUrl: '/admin/dict/type/scm_yes_no'
            }
            , {
              label: '是否限制下单',
              type: 'radio',
              prop: 'needConfirmation',
              labelWidth: 120,
              // value: '0',
              dicUrl: '/admin/dict/type/scm_yes_no'
            }, {
              label: '排序值',
              type: 'number',
              maxlength: 5,
              prop: 'sort',
              value: '100',
              showWordLimit: true,
              // dicUrl: '/admin/dict/type/scm_supplier_level'
            }, {
              label: '账期日',
              type: 'number',
              prop: 'payDay',
              value: '1',
              maxlength: 2,
              showWordLimit: true,
              // dicUrl: '/admin/dict/type/scm_supplier_level'
            }, {
              label: '是否需下单支付',
              type: 'radio',
              prop: 'needPay',
              maxlength: 2,
              labelWidth: 120,
              showWordLimit: true,
              dicUrl: '/admin/dict/type/scm_yes_no'
            }
            ]
          }]
      }

      