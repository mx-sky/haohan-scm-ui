/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 100,
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  viewBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '店铺名称',
      prop: 'name',
      sortable: true,
      search: true
    },
    {
      label: '商家名称',
      prop: 'merchantName',
      sortable: true,
      search: true
    },
    {
      label: '店铺地址',
      prop: 'address',
    },
    {
      label: '店铺负责人',
      prop: 'manager',
    },
    {
      label: '联系电话',
      prop: 'telephone',
    }, {
      label: '店铺描述',
      prop: 'shopDesc',
    },{
      label: '服务内容',
      prop: 'shopService',
    }, {
      label: '店铺等级',
      prop: 'shopLevel',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/shop_level'
    }, {
      label: '认证状态',
      prop: 'authType',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/auth_status'
    }, {
      label: '审核状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/merchant_type'
    }
  ]
}


