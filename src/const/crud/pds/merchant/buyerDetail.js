export const detailOption = {
        addBtn:false,
        group: [
          {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
              {
                label: '商家名称',
                prop: 'merchantName',
              },
              {
                label: '采购商全称',
                prop: 'buyerName'
              },
              {
                label: '采购商简称',
                prop: 'shortName'
              },
              
              {
              label: '联系人',
              prop: 'contact'
            }, {
              label: '联系电话',
              prop: 'telephone'
            }, {
              label: '部门名称',
              prop: 'deptName'
              
            }, {
              label: '备注',
              prop: 'remarks'
            }
            ,{
              label: '角色',
              prop: 'roleList',
              type:'select',
              props: {
                  label: 'roleName',
                  value: 'roleId'
              },
              dicUrl:'/admin/role/list'
            },
            {
              label: '地址定位',
              prop: 'area',
              span: 6
            }, {
              label: '采购商地址',
              prop: 'address',
              span: 12
            },{
              label: '地址定位 ',
              prop: 'position',
              span: 12
            }
            ,{
              label: '创建时间',
              prop: 'createDate',
              minWidth: 110,
              search: true
            },
            ]
          },
          {
            icon: 'el-icon-info',
            label: '扩展信息',
            prop: 'group',
            column: [{
              label: '账期',
              prop: 'payPeriod',
              type: 'radio',
              dicUrl: '/admin/dict/type/scm_pay_period'
            }, {
              label: '消息推送',
              type: 'radio',
              prop: 'needPush',
              dicUrl: '/admin/dict/type/scm_yes_no'
            }, {
              label: '排序值',
              maxlength: 5,
              prop: 'sort',
            }, {
              label: '账期日',
              prop: 'payDay',
              maxlength: 2,
            },{
              label: '采购商类型',
              prop: 'buyerType',
              type: 'radio',
              dicUrl: '/admin/dict/type/scm_buyer_type'
            }, {
              label: '启用状态',
              type: 'radio',
              prop: 'status',
              dicUrl: '/admin/dict/type/scm_use_status'
            }
            , {
              label: '是否限制下单',
              type: 'radio',
              prop: 'needConfirmation',
              // value: '0',
              dicUrl: '/admin/dict/type/scm_yes_no'
            },
            {
              label: '是否需下单支付',
              prop: 'needPay',
              type: 'select',
              searchLabelWidth: 120,
              minWidth: 140,
              search: true,
              dicUrl: '/admin/dict/type/scm_yes_no'
            }]
          }
        ]
      }