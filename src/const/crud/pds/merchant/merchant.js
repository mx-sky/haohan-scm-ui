/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 100,
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  viewBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '商家名称',
      prop: 'merchantName',
      sortable: true,
      search: true
    },
    {
      label: '联系人',
      prop: 'contact',
      search: true
    },
    {
      label: '联系电话',
      prop: 'telephone',
    },
    {
      label: '地址',
      prop: 'address',
    },
    {
      label: '业务介绍',
      prop: 'bizDesc',
    },
    {
      label: '行业',
      prop: 'industry',
    },
    {
      label: '启用状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/merchant_type',
      slot:true
    }
  ]
}


