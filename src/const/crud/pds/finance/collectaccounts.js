/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    showSummary: true,
    indexLabel: '单号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 100,
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    viewBtn: false,
    delBtn: false,
    addBtn: false,
    editBtn: false,
    selection: true,
    sumColumnList: [
      {
        name: 'buyerPayment',
        type: 'sum'
      },
    ],
    column: [
        {
            label: '账单编号',
            prop: 'buyerPaymentId',
            sortable: true,
            search: true
        },
        {
            label: '订单编号',
            prop: 'receivableSn',
            sortable: true,
            search: true
        },
        {
            label: '客户名称',
            prop: 'customerName',
            sortable: true,
            search: true
        },
        {
            label: '商家名称',
            prop: 'merchantName',
            sortable: true,
            search: true
        },
        {
            label: '单据金额',
            prop: 'buyerPayment',
        }, {
            label: '类型',
            prop: 'billType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_bill_type',
            sortable: true,
        }, {
            label: '是否结算',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            sortable: true,
            search: true
        },{
            label: '审核状态',
            prop: 'reviewStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_review_status',
            sortable: true,
            search: true
        },{
            label: '成交时间',
            prop: 'buyDate',
            type: 'date',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            sortable: true,
        },
        {
            label: '客户订单编号',
            prop: 'buyId',
            hide: true
        }
    ]
}


