export const detailOption = {
  detail: true,
  group: [
    {
      icon: 'el-icon-info',
      label: '商品信息',
      prop: 'group1',
      column: [
        {
          label: '商品名称',
          prop: 'goodsName',
          span: 8
        },
        {
          label: '商品唯一编号',
          prop: 'goodsSn',
          span: 8
        },
        {
          label: '排序值(权重)',
          prop: 'sort',
          span: 8
        },
        {
          label: '计量单位',
          prop: 'unit',
          span: 8
        },
        {
          label: '上下架',
          prop: 'isMarketable',
          span: 8,
          dicUrl: '/admin/dict/type/scm_yes_no',
          // formslot: true
        },
        {
          label: '搜索关键词',
          prop: 'simpleDesc',
          type: 'text',
          span: 8
        },
        {
          label: '展示分类',
          prop: 'categoryName',
          span: 8
        },
        {
          label: '商品主图',
          prop: 'thumbUrl',
          span: 24,
          formslot: true
        },
        {
          label: '商品轮播图',
          prop: 'photoList',
          span: 24,
          formslot: true
        },
        // {
        //   label: "商品描述",
        //   prop: 'detailDesc',
        //   span: 24,
        // }
      ]
    },
    {
      icon: 'el-icon-info',
      label: '价格库存',
      prop: 'group2',
      column: [
        {
          label: '库存',
          prop: 'storage'
        },
        {
          label: '市场价格',
          prop: 'marketPrice'
        },
        {
          label: "会员价",
          prop: "vipPrice"
        },
        {
          label: "虚拟价格",
          prop: 'virtualPrice'
        }
      ]
    }
  ]
}
export const tableOption = {
  title: '订单商品明细',
  page: false,
  border: true,
  index: true,
  indexWidth: 50,
  indexLabel: "序号",
  maxHeight: 400,
  stripe: true,
  header: false,
  align: "center",
  menu: false,
  column: [
    {
      label: '规格名称',
      prop: 'modelName',
    },
    {
      label: '商品图片',
      prop: 'modelUrl',
      type: 'img',
      dataType: 'string'
    },
    {
      label: '价格',
      prop: 'modelPrice'
    },
    {
      label: '虚拟价',
      prop: 'virtualPrice'
    },
    {
      label: '参考成本价',
      prop: 'costPrice'
    },
    {
      label: '库存',
      prop: 'modelStorage'
    },
    {
      label: '单位',
      prop: 'modelUnit'
    },
    {
      label: '重量',
      prop: 'weight'
    },
    {
      label: '体积',
      prop: 'volume'
    },
    {
      label: '库存预警值',
      prop: 'stocksForewarn'
    },
    {
      label: '扫码购编码',
      prop: 'modelCode'
    }
  ]
}
