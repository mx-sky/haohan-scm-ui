export const editOption = {
  emptyBtn: false,
  detail: false,
  // menuBtn:false,
  group: [
    {
      label: '基本信息',
      prop: 'group1',

      column: [
        {
          label: '供应商名称',
          prop: 'supplierName',
          maxlength: 10,
          labelWidth: 120,
          showWordLimit: true,
          rules: [{
            required: true,
            message: '请输入供应商名称',
            trigger: 'blur'
          }]

        }, {
          label: '供应商简称',
          prop: 'shortName',
          maxlength: 10,
          labelWidth: 120,
          showWordLimit: true,
          rules: [{
            required: true,
            message: '请输入供应商简称',
            trigger: 'blur'
          }]
        }, {
          label: '商家',
          prop: 'merchantId',
          maxlength: 10,
          formslot: true,
        }, {
          label: '联系人',
          prop: 'contact',
          maxlength: 10,
          showWordLimit: true,
          rules: [{
            required: true,
            message: '请输入联系人',
            trigger: 'blur'
          }]
        }, {
          label: '电话',
          prop: 'telephone',
          "type": "number",
          // precision:Number,
          precision: 0,
          maxlength: 11,
          minlength: 11,
          showWordLimit: true,
          rules: [{
            required: true,
            message: '请输入电话',
            trigger: 'blur',
          }]
        },
        {
          label: '详细地址',
          prop: 'address',
          showWordLimit: true,
          formslot: true

        },
        {
          "type": "input",
          "label": "权限角色",
          "prop": "roleIds",
          showWordLimit: true,
          formslot: true
          // span: 8
        }, {
          label: '标注位置 ',
          prop: 'position',
          showWordLimit: true,
          formslot: true
          // span: 8
        },

        {
          label: '部门',
          prop: 'deptId',
          formslot: true,

        },
        {
          label: '区域',
          prop: 'area',
          formslot: true
        }, {
          label: '备注',
          prop: 'remarks',
          type: 'textarea',
          maxlength: 200,
          showWordLimit: true
        }, {
          label: '标签',
          prop: 'tags',
          // type: 'textarea',
          // maxlength: 200,
          showWordLimit: true
        }]
    }, {
      label: '扩展信息',
      prop: 'group2',
      column: [{
        label: '供应商类型',
        prop: 'supplierType',
        type: 'radio',
        value: '0',
        dicUrl: '/admin/dict/type/scm_supplier_type'
      }, {
        label: '启用状态',
        type: 'radio',
        prop: 'status',
        value: '1',
        dicUrl: '/admin/dict/type/scm_use_status'
      }, {
        label: '账期',
        type: 'radio',
        prop: 'payPeriod',
        value: '1',
        dicUrl: '/admin/dict/type/scm_pay_period'
      }, {
        label: '供应商等级',
        type: 'radio',
        prop: 'supplierLevel',
        value: '1',
        dicUrl: '/admin/dict/type/scm_supplier_level'
      }, {
        label: '消息推送',
        type: 'radio',
        prop: 'needPush',
        value: '0',
        dicUrl: '/admin/dict/type/scm_yes_no'
      }, {
        label: '排序值',
        type: 'number',
        maxlength: 5,
        prop: 'sort',
        value: '100',
        showWordLimit: true,
        // dicUrl: '/admin/dict/type/scm_supplier_level'
      }, {
        label: '账期日',
        type: 'number',
        prop: 'payDay',
        value: '1',
        maxlength: 2,
        showWordLimit: true,
        // dicUrl: '/admin/dict/type/scm_supplier_level'
      }
      ]
    }]
}
//供应商编辑中的平台商品
export const modelGoodsOption = {
  title: '供应商品的规格',
  addBtn: false,
  page: false,
  border: true,
  index: true,
  indexWidth: 50,
  indexLabel: "序号",
  maxHeight: 400,
  stripe: true,
  header: false,
  align: "center",
  cellBtn: true,
  editBtn: false,
  delBtn: false,
  column: [
    {
      label: 'typeName0',
      prop: 'typeName0',
      slot: true,
      hide: true,
      width: "120"
    },
    {
      label: 'typeName1',
      prop: 'typeName1',
      slot: true,
      hide: true,
      width: "120"
    },
    {
      label: 'typeName2',
      prop: 'typeName2',
      slot: true,
      hide: true,
      width: "120"
    },
    {
      label: '图片',
      prop: 'modelUrl',
      slot: true
    },
    {
      label: '价格',
      prop: 'modelPrice',
      type: 'number',
      cell: true,
      width: "120",
      precision: 2
    },
    {
      label: '虚拟价',
      prop: 'virtualPrice',
      type: 'number',
      cell: true,
      width: "120",
      precision: 2
    },
    {
      label: '参考成本价',
      prop: 'costPrice',
      type: 'number',
      cell: true,
      width: "120",
      precision: 2
    },
    {
      label:'库存',
      prop: 'modelStorage',
      type: 'number',
      cell: true,
      width: "120"
    },
    {
      label: '单位',
      prop: 'modelUnit',
      type: 'input',
      cell: true,
      width: "120"
    },
    {
      label: '重量',
      prop: 'weight',
      type: 'number',
      cell: true,
      width: "120",
      precision: 3
    },
    {
      label: '体积',
      prop: 'volume',
      type: 'number',
      cell: true,
      width: "120",
      precision: 6
    },
    {
      label: '库存预警值',
      prop: 'stocksForewarn',
      type: 'number',
      cell: true,
      width: "120"
    },
    {
      label: '扫码购编码',
      prop: 'modelCode',
      type: 'input',
      cell: true,
      width: "250"
    }
  ]
}
