/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '供应商名称',
            prop: 'supplierName',
            search:true
        },
        {
            label: '供应类型',
            prop: 'supplyType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_supply_type '
        },
        {
            label: '供应商品名称',
            prop: 'goodsName',
            search:true

        },
        {
            label: '协议生效时间',
            prop: 'effectTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '协议截止时间',
            prop: 'deadlineTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '协议定价说明',
            prop: 'agreementDesc'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
