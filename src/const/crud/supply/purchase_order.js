export const tableOption = {
    "border": true,
    "index": true,
    "selection":true,
    // indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [{
        label: '采购单编号',
        prop: 'buyOrderSn',
        minWidth: 100,
        search: true,
        fixed:true,
        searchLabelWidth: 100,
    },{
        label: '采购商名称',
        prop: 'buyerName',
        minWidth: 100,
        search: true,
        searchLabelWidth: 100,
    },{
        label: '状态',
        prop: 'status',
        type: 'select',
        dicUrl: '/admin/dict/type/scm_buy_order_status',
        minWidth: 100,
        search: true
    },{
        label: '批次',
        type: 'select',
        dicUrl: '/admin/dict/type/scm_delivery_seq',
        prop: 'buySeq',
        minWidth: 100,
        search: true
    },{
        label: '配送日期',
        prop: 'deliveryDate',
      type: "datetime",
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd",
        minWidth: 100,
        search: true
    },{
        label: '配送方式',
        prop: 'deliveryType',
        dicUrl: '/admin/dict/type/scm_delivery_type',
        minWidth: 100,
        // search: true
    },{
        label: '总价预估',
        prop: 'genPrice',
        minWidth: 100,
        // search: true
    },{
        label: '供应下单结果',
        prop: 'supplyMsg',
        minWidth: 120,
        // searchLabelWidth: 120,
    },{
        label: '供应订单编号',
        prop: 'supplySn',
        minWidth: 120,
        slot: true,
        // searchLabelWidth: 120,
    }
    ,
    {
        label: '支付状态',
        prop: 'payStatus',
        minWidth: 120,
        dicUrl: '/admin/dict/type/scm_order_pay_status',
        // searchLabelWidth: 120,
    }
    ,{
        label: '备注',
        prop: 'remarks',
        minWidth: 100,
    },{
        label: '下单时间',
        prop: 'buyTime',
      type: "datetime",
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd",
        minWidth: 100,
    },{
        label: '联系人',
        prop: 'contact',
        minWidth: 100,
        // search: true
    },{
        label: '电话',
        prop: 'telephone',
        minWidth: 120,
        // search: true
    },
]
}