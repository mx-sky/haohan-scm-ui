/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '供应商名称',
            prop: 'supplierName',
            search:true
        },
        {
            label: '等级',
            prop: 'gradeType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_grade_type '
        },
        {
            label: '评价人',
            prop: 'evaluatorId'
        },
        {
            label: '评价人名称',
            prop: 'evaluatorName'
        },
        {
            label: '评价时间',
            prop: 'evaluateTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '评价说明',
            prop: 'evaluateDesc'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
