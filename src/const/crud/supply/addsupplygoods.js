export const addoption = {
  labelWidth: 100,
  submitBtn: false,
  emptyBtn: false,
  group: [
    {
      label: '商品信息',
      prop: 'group1',
      column: [
        {
          label: '商品名称',
          prop: 'goodsName',
          span: 8,
          rules: [
            {
              required: true,
              message: '请输入商品名称',
              trigger: 'blur'
            }
          ],
          maxlength: 30,
          showWordLimit: true
        },
        {
          label: '排序值(权重)',
          prop: 'sort',
          span: 8,
          type: 'number'
        },
        {
          label: '商品状态',
          prop: 'goodsStatus',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_goods_status',
          rules: [
            {
              required: true,
              message: '请选择商品状态'
            }
          ],
          span: 8
        },
        {
          label: '计量单位',
          prop: 'unit',
          rules: [
            {
              required: true,
              message: '请输入计量单位',
              trigger: 'blur'
            }
          ],
          span: 8
        },

        {
          label: 'C端销售',
          prop: 'salecFlag',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          display: false,
          span: 8
        },
        {
          label: '商品类型',
          prop: 'goodsType',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_goods_type',
          span: 8
        },
        {
          label: '商品来源',
          prop: 'goodsFrom',
          span: 8,
          display: false
        },
        {
          label: '赠品标记',
          prop: 'goodsGift',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '售卖规则',
          prop: 'saleRule',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '配送规则',
          prop: 'deliveryRule',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '服务选项',
          prop: 'serviceSelection',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '选择店铺',
          prop: 'category',
          span: 16,
          row: true,
          formslot: true
        },
        {
          label: '扫码购编码',
          prop: 'scanCode',
          span: 8
        },
        {
          label: '商品描述',
          prop: 'detailDesc',
          type: 'textarea',
          minRows: 2,
          maxlength: 200,
          showWordLimit: true,
          span: 8,
          display: false
        },
        {
          label: '搜索关键词',
          prop: 'simpleDesc',
          type: 'textarea',
          minRows: 2,
          maxlength: 200,
          showWordLimit: true,
          span: 8
        },

        {
          label: '商品主图',
          prop: 'thumbUrl',
          row: true,
          formslot: true,
          // rules: [
          //   {
          //     required: true,
          //     message: '请选择商品主图'
          //   }
          // ],
        },
        {
          label: '商品轮播图',
          prop: 'photoList',
          formslot: true,
          span: 16
        }]
    },
    {
      label: '商品信息',
      prop: 'group2',
      column: [
        {
          label: '市场价',
          prop: 'marketPrice',
          type: 'number',
          rules: [
            {
              required: true,
              message: '请输入市场价',
              trigger: 'blur'
            }
          ],
        },
        {
          label: '虚拟价格',
          prop: 'virtualPrice',
          type: 'number'
        },
        {
          label: '会员价',
          prop: 'vipPrice',
          type: 'number'
        },
        {
          label: '库存',
          prop: 'storage',
          type: 'number',
          row: true
        }
      ]
    }
  ]
}
export const goodsOption = {
  labelWidth: 100,
  submitBtn: false,
  emptyBtn: false,
  group: [
    {
      label: '商品信息',
      prop: 'group1',
      column: [
        {
          label: '商品名称',
          prop: 'goodsName',
          span: 8,
          rules: [
            {
              required: true,
              message: '请输入商品名称',
              trigger: 'blur'
            }
          ],
          maxlength: 30,
          showWordLimit: true
        },
        {
          label: '排序值(权重)',
          prop: 'sort',
          span: 8,
          type: 'number'
        },
        {
          label: '商品状态',
          prop: 'goodsStatus',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_goods_status',
          rules: [
            {
              required: true,
              message: '请选择商品状态'
            }
          ],
          span: 8
        },
        {
          label: '计量单位',
          prop: 'unit',
          rules: [
            {
              required: true,
              message: '请输入计量单位',
              trigger: 'blur'
            }
          ],
          span: 8
        },

        {
          label: 'C端销售',
          prop: 'salecFlag',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          rules: [
            {
              required: true,
              message: '请选择C端销售'
            }
          ],
          disabled: true,
          span: 8
        },
        {
          label: '商品类型',
          prop: 'goodsType',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_goods_type',
          span: 8
        },
        {
          label: '商品来源',
          prop: 'goodsFrom',
          span: 8,
          display: false
        },
        {
          label: '赠品标记',
          prop: 'goodsGift',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '售卖规则',
          prop: 'saleRule',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '配送规则',
          prop: 'deliveryRule',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '服务选项',
          prop: 'serviceSelection',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_yes_no',
          span: 8,
          display: false
        },
        {
          label: '分类展示',
          prop: 'category',
          span: 8,
          row: true,
          formslot: true,
          // rules: [
          //   {
          //     required: true,
          //     message: '请选择分类'
          //   }
          // ],
        },
        {
          label: '扫码购编码',
          prop: 'scanCode',
          span: 8
        },
        {
          label: '商品描述',
          prop: 'detailDesc',
          type: 'textarea',
          minRows: 2,
          maxlength: 200,
          showWordLimit: true,
          span: 8,
          display: false
        },
        {
          label: '搜索关键词',
          prop: 'simpleDesc',
          type: 'textarea',
          minRows: 2,
          maxlength: 200,
          showWordLimit: true,
          span: 8
        },

        {
          label: '商品主图',
          prop: 'thumbUrl',
          row: true,
          formslot: true,
          rules: [
            {
              required: true,
              message: '请选择商品主图',
            }
          ],
        },
        {
          label: '商品轮播图',
          prop: 'photoList',
          formslot: true,
          span: 16
        }]
    },
    {
      label: '商品信息',
      prop: 'group2',
      column: [
        {
          label: '市场价',
          prop: 'marketPrice',
          type: 'number',
          rules: [
            {
              required: true,
              message: '请输入市场价',
              trigger: 'blur'
            }
          ],
        },
        {
          label: '虚拟价格',
          prop: 'virtualPrice',
          type: 'number'
        },
        {
          label: '会员价',
          prop: 'vipPrice',
          type: 'number'
        },
        {
          label: '库存',
          prop: 'storage',
          type: 'number',
          row: true
        }
      ]
    }
  ]
}
