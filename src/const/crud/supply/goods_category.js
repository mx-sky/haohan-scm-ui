export const tableOption = {
  addBtn: false,
  editBtn: false,
  delBtn: false,
  headerAlign: 'center',
  align: 'center',
  border: true,
  index: true,
  defaultExpandAll:true,
  searchMenuSpan:8,
  column: [
    {
      label: '分类名称',
      prop: 'name',
      align: 'left',
      width: 200,
      // search:true,
      // filterable: true,
      // searchFilterable: true
    },
    {
      label: "分类图标",
      prop: 'logo',
      slot: true
    },
    {
      label: "排序值",
      prop: 'sort',
    },
    {
      label: '批量上下架',
      prop: 'batch',
      slot: true
    },
    {
      label: '是否显示',
      prop: 'show',
      slot: true
    },
    {
      label: "新增子分类",
      prop: 'children',
      slot: true
    }
  ]
}
export const supplyTableOption = {
  addBtn: false,
  editBtn: false,
  delBtn: false,
  headerAlign: 'center',
  align: 'center',
  border: true,
  index: true,
  defaultExpandAll:true,
  menu: false,
  // searchMenuSpan:8,
  column: [
    {
      label: '分类名称',
      prop: 'name',
      align: 'left',
      width: 200,
    },
    {
      label: "分类图标",
      prop: 'logo',
      slot: true
    },
    {
      label: "排序值",
      prop: 'sort',
    },
    {
      label: '批量上下架',
      prop: 'batch',
      slot: true
    },
    {
      label: '是否显示',
      prop: 'show',
      slot: true
    },
  ]
}
