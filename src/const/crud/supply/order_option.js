/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const orderOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    expand: true,
    expandRowKeys: [1],
    rowKey: 'id',
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    border: false,
    column: [
        // {
        //     label: '主键',
        //     prop: 'id'
        // },
        {
            label: '订单号',
            prop: 'supplySn',
            minWidth: 100,
            search: true
        },
        {
            label: '购买人',
            prop: 'contact',
            search: true,
        },
        {
            label: '渠道',
            prop: 'billType',
            search: true,
        },
        {
            label: '状态',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_buy_order_status',
            search: true,
        },
        {
            label: '供应商',
            prop: 'supplierName',
            search: true,
        }, {

            label: '下单时间',
            prop: 'orderTime',
            search: true,
        }, {
            label: '供应日期',
            prop: 'supplyDate',
            search: true,
        }, {
            label: '联系电话',
            prop: 'telephone',
            search: true,
        }, {
            label: '供应地址',
            prop: 'address',
            // search: true,
        }, {
            label: '成交时间',
            prop: 'dealTime',
            search: true,
        }, {
            label: '配送方式',
            prop: 'deliveryType',
            dicUrl: '/admin/dict/type/scm_delivery_type',
            search: true,
        }

    ]
}
