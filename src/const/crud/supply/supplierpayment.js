/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '询价单号',
            prop: 'askOrderId'
        },
        {
            label: '供应商',
            prop: 'supplierId',
            search:true
        },
        {
            label: '供应日期',
            prop: 'supplyDate',
            type: "datetime"
        },
        {
            label: '商品数量',
            prop: 'goodsNum'
        },
        {
            label: '供应货款',
            prop: 'supplierPayment'
        },
        {
            label: '售后货款',
            prop: 'afterSalePayment'
        },
        {
            label: '是否结算',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_yes_no '
        },
        {
            label: '售后单编号列表',
            prop: 'serviceId'
        },
        {
            label: '供应商货款记录编号',
            prop: 'supplierPaymentId'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
