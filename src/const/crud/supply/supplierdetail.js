export const detailOption = {
  addBtn:false,
  group: [
    {
      icon: 'el-icon-info',
      label: '基础信息',
      prop: 'group1',
      column: [
        {
          label: '商家名称',
          prop: 'merchantName',
        },
        {
          label: '供应商全称',
          prop: 'supplierName'
        },
        {
          label: '供应商简称',
          prop: 'shortName'
        },
        
        {
        label: '联系人',
        prop: 'contact'
      }, {
        label: '联系电话',
        prop: 'telephone'
      }, {
        label: '部门名称',
        prop: 'deptName'
        
      }, {
        label: '标签',
        prop: 'tags'
      }, {
        label: '备注',
        prop: 'remarks'
      }
      ,{
        label: '角色',
        prop: 'roleList',
        type:'select',
        props: {
            label: 'roleName',
            value: 'roleId'
        },
        dicUrl:'/admin/role/list'
      }, {
        label: '供应商地址',
        prop: 'address',
        span: 12
      },{
        label: '地址定位 ',
        prop: 'position',
        span: 12
      },{
        label: '地址定位',
        prop: 'area',
        span: 4
      }]
    },
    {
      icon: 'el-icon-info',
      label: '扩展信息',
      prop: 'group',
      column: [{
        label: '账期',
        prop: 'payPeriod',
        type: 'radio',
        dicUrl: '/admin/dict/type/scm_pay_period'
      }, {
        label: '供应商等级',
        type: 'radio',
        prop: 'supplierLevel',
        dicUrl: '/admin/dict/type/scm_supplier_level'
      }, {
        label: '消息推送',
        type: 'radio',
        prop: 'needPush',
        dicUrl: '/admin/dict/type/scm_yes_no'
      }, {
        label: '排序值',
        maxlength: 5,
        prop: 'sort',
      }, {
        label: '账期日',
        prop: 'payDay',
        maxlength: 2,
      },{
        label: '供应商类型',
        prop: 'supplierType',
        type: 'radio',
        dicUrl: '/admin/dict/type/scm_supplier_type'
      }, {
        label: '启用状态',
        type: 'radio',
        prop: 'status',
        dicUrl: '/admin/dict/type/scm_use_status'
      }]
    }
  ]
}