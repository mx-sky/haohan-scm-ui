/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 收货相关和订单相关信息
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '收货相关',
            prop: 'group1',
            column: [
                {
                    label: '供应商名称',
                    prop: 'supplierName',
                    span: 6
                },
                {
                    label: '供应订单编号',
                    prop: 'supplySn',
                    span: 6
                },
                {
                    label: '联系人',
                    prop: 'contact',
                    span: 6
                },
                {
                    label: '联系电话',
                    prop: 'telephone',
                    span: 6
                },
                {
                    label: '供应地址',
                    prop: 'address',
                    span: 6
                },
                {
                    "label": "成交时间",
                    "prop": "dealTime",
                    "type": "date",
                    format: "yyyy-MM-dd",
                    span: 6
                },
                {
                    "label": "供应日期",
                    "prop": "supplyDate",
                    "type": "date",
                    format: "yyyy-MM-dd",
                    span: 6
                },
                {
                    "label": "下单时间",
                    "prop": "orderTime",
                    "type": "date",
                    format: "yyyy-MM-dd",
                    span: 6
                },
                {
                    label: '订单状态',
                    prop: 'status',
                    dicUrl: '/admin/dict/type/scm_buy_order_status',
                    span: 6
                },
                {
                    label: '配送方式',
                    prop: 'deliveryType',
                    dicUrl: '/admin/dict/type/scm_delivery_type',
                    span: 6
                },
                {
                    label: '其他金额',
                    prop: 'otherAmount',
                    span: 6
                },
                {
                    label: '供应总金额',
                    prop: 'totalAmount',
                    span: 6
                },
                {
                    label: '商品合计金额',
                    prop: 'sumAmount',
                    span: 6
                },
                {
                    label: '需求描述',
                    prop: 'needNote',
                    span: 6
                }
            ]
        },
    ]
}
