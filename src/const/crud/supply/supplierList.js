/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '#',
  stripe: true,
  menuAlign: 'center',
  // menuWidth: 200,
  // size:"mini",
  delBtn: false,
  align: 'center',
  viewBtn: false,
  addBtn: false,
  editBtn: false,
  emptySize:'mini',
  column: [
    {
      label: '供应商名称',
      prop: 'supplierName',
      sortable: true,
      search: true,
      minWidth: 110,

    },
    {
      label: '简称',
      prop: 'shortName',
      minWidth: 100,
    },
    {
      label: '星级',
      prop: 'supplierLevel',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_grade_type'
    }, {
      label: '联系人',
      prop: 'contact',
      minWidth: 120,
      search: true,
    },
    {
      label: '联系电话',
      prop: 'telephone',
      'type': 'number',
      minWidth: 120,
      search: true,
    },
    {
      label: '账期',
      prop: 'payPeriod',
      search: true,
      dicUrl: '/admin/dict/type/scm_pay_period'
    }, {
      label: '供应商地址',
      prop: 'address',
      overHidden: true,
      minWidth: 110,
    }, {
      label: '商家名称',
      prop: 'merchantName',
      search: true
    }, {
      label: '供应商类型',
      prop: 'supplierType',
      type: 'select',
      minWidth: 110,
      search: true,
      dicUrl: '/admin/dict/type/scm_supplier_type'
    }, {
      label: '启用状态',
      prop: 'status',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_use_status'
    }, {
      label: '消息推送',
      prop: 'needPush',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_yes_no'
    },
    {
      label: '排序值',
      prop: 'sort'
    }
  ]
}


