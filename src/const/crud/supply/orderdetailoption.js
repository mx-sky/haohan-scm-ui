export const detailOption = {
  group: [
    {
      icon: 'el-icon-info',
      label: '基础信息',
      prop: 'group',
      column: [{
        label: '销售订单编号',
        prop: 'salesOrderSn',
        span: 6
      }, {
        label: '客户编号',
        prop: 'customerSn',
        span: 6
      }, {
        label: '类型',
        prop: 'sales_type',
        span: 6
      }, {
        label: '其它金额',
        prop: 'otherAmount',
        span: 6
      }, {
        label: '合计金额',
        prop: 'sumAmount',
        span: 6
      }, {
        label: '总计金额',
        prop: 'totalAmount',
        span: 6
      }, {
        label: '优惠金额',
        prop: 'couponAmount',
        span: 6
      }, {
        label: '业务人名称',
        prop: 'linkmanName',
        span: 6
      }, {
        label: '地址',
        prop: 'address',
        span: 6
      }]
    },
    {
      icon: 'el-icon-info',
      label: '订单商品清单'
    }
  ]
}
export const crudOption = {
  index: true,
  indexLabel: '序号',
  addBtn: false,
  refreshBtn: false,
  columnBtn: false,
  menu: false,
  align: 'center',
  menuAlign: 'center',
  simplePage: true,
  border: true,
  header: false,
  column: [
    {
      label: '商品图片',
      prop: 'goodsImg',
      type: 'img',
      dataType: 'string'
    }, {
      label: '商品名称',
      prop: 'goodsName'
    }, {
      label: '规格名称',
      prop: 'modelName'

    }, {
      label: '单位',
      prop: 'unit'
    }, {
      label: '订单数量',
      prop: 'goodsNum'
    }, {
      label: '发货单价',
      prop: 'dealPrice'
    },
    //  {
    //   label: '小计金额',
    //   prop: 'dealPrice'
    // },
     {
      label: '备注',
      prop: 'remarks'
    }
  ]
}
