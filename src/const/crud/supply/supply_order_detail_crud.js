/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    title: '订单商品明细',
    page: false,
    border: true,
    index: true,
    indexWidth: 50,
    indexLabel: "序号",
    // height: 400,
    stripe: true,
    menu: false,
    header: false,
    align: "center",
    column: [
        {
            label: '商品图片',
            prop: 'goodsImg',
            type:'img',
            minWidth: 100,
            dataType: 'string',
            imgWidth:80,
            imgHeight:80,
            // listType:'picture-img'
        },
        {
            type: 'input',
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            type: 'input',
            label: '规格',
            prop: 'modelName',

        },
        {
            type: 'input',
            label: '单位',
            prop: 'unit',

        },
        {
            type: 'input',
            label: '订单明细编号',
            prop: 'supplyDetailSn',
        
        },
        {
            type: 'input',
            label: '成交价格',
            prop: 'dealPrice',

        },
        {
            type: 'input',
            label: '采购数量',
            prop: 'goodsNum',

        },
        {
            type: "number",
            label: '商品金额',
            prop: 'amount',
            precision: 2,
            valueDefault: 0,
        },
        {
            type: 'select',
            label: '商品规格编号',
            prop: 'goodsModelSn',
        },

    ]
}
