/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

// 发货记录 可修改属性
export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '发货相关',
            prop: 'group2',
            column: [
                {
                    label: '发货人',
                    prop: 'shipperName',
                    formslot: true,
                    span: 8
                },
                {
                    label: '发货人电话',
                    prop: 'shipperTelephone',
                    readonly: true,
                    span: 8,
                    placeholder: '请选择发货人'
                },
                {
                    label: '发货地址',
                    prop: 'shipAddress',
                    readonly: true,
                    span: 8,
                    placeholder: '请选择发货人'
                },
                {
                    label: '发货方式',
                    prop: 'shipType',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_ship_type',
                    clearable: false,
                    span: 8
                },
                {
                    label: '物流单号',
                    prop: 'logisticsSn',
                    span: 8
                },
                {
                    label: '物流公司',
                    prop: 'logisticsName',
                    span: 8
                },
                {
                    "label": "发货时间",
                    "prop": "shipTime",
                    "type": "datetime",
                    format: "yyyy-MM-dd HH:mm:ss",
                    valueFormat: "yyyy-MM-dd HH:mm:ss",
                    display:false,
                    clearable: false,
                    span: 6
                },
                {
                    label: '发货状态',
                    prop: 'shipStatus',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_ship_record_status',
                    display:false,
                    clearable: false,
                    span: 6
                },
            ]
        },
    ]
}
