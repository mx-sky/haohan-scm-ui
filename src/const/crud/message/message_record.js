/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        // {
        //     label: '主键',
        //     prop: 'id',
        //     hide: true,
        //     width:100
        // },
        {
            label: '消息编号',
            prop: 'messageSn',
            minWidth: 100,
            search: true
        },
        {
            label: '标题',
            prop: 'title',
            minWidth: 100,
            search: true
        },
        {
            label: '消息类型',
            prop: 'messageType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_message_type',
            search: true,
        },
        {
            label: '发送人',
            prop: 'senderName',
            search: true
        },
        {
            label: '业务部门',
            prop: 'departmentType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_department_type',
            search: true,
        },
        {
            label: '业务类型',
            prop: 'msgActionType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_msg_action_type',
            search: true,
        },
        {
            "label": "发送时间",
            "prop": "sendTime",
            width: 180,
        },
        {
            label: "发送时间",
            prop: "sendTimeRange",
            format: "yyyy-MM-dd",
            type: 'date',
            valueFormat: "yyyy-MM-dd",
            search: true,
            searchRange: true,
            searchSpan: 12,
            hide: true,
        },
    ]
}
