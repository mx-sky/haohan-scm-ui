/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家ID',
      prop: 'pmId'
    },
	  {
      label: '车辆编号',
      prop: 'truckNo'
    },
	  {
      label: '车辆品牌',
      prop: 'brand'
    },
	  {
      label: '名称',
      prop: 'truckName'
    },
	  {
      label: '负责人',
      prop: 'principal'
    },
	  {
      label: '司机',
      prop: 'driver'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '车辆载重Kg',
      prop: 'carryWeight'
    },
	  {
      label: '车辆容积m3',
      prop: 'carryVolume'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
