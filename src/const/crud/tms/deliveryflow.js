/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家ID',
      prop: 'pmId'
    },
	  {
      label: '配送编号',
      prop: 'deliveryId'
    },
	  {
      label: '配送日期',
      prop: 'deliveryDate'
    },
	  {
      label: '送货批次',
      prop: 'deliverySeq'
    },
	  {
      label: '物流车号',
      prop: 'truckNo'
    },
	  {
      label: '司机',
      prop: 'driver'
    },
	  {
      label: '线路编号',
      prop: 'lineNo'
    },
	  {
      label: '当日车次',
      prop: 'ondayTrains'
    },
	  {
      label: '装车时间',
      prop: 'loadTruckTime'
    },
	  {
      label: '发车时间',
      prop: 'departTruckTime'
    },
	  {
      label: '完成时间',
      prop: 'finishTime'
    },
	  {
      label: '货物数量',
      prop: 'goodsNum'
    },
	  {
      label: '重量',
      prop: 'goodsWeight'
    },
	  {
      label: '体积',
      prop: 'goodsVolume'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
