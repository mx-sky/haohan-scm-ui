/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn:false,
  editBtn:false,
  addRowBtn:false,
  cellBtn:false,
  viewBtn:false,
  delBtn:false,
  column: [
	  {
      label: '商家',
      prop: 'merchantId'
    },
	  {
      label: '店铺',
      prop: 'shopId'
    },
	  {
      label: '应用ID',
      prop: 'clientId',
      hide:true
    },
	  {
      label: '打印机编号',
      prop: 'machineCode',
      search: true
    },
	  {
      label: '密钥',
      prop: 'secret'
    },
	  {
      label: '打印机名称',
      prop: 'name',
      search: true
    },
	  {
      label: '状态',
      prop: 'status',
      dicUrl:'/admin/dict/type/scm_yes_no',
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
