/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn:false,
  editBtn:false,
  addRowBtn:false,
  cellBtn:false,
  viewBtn:false,
  delBtn:false,
  column: [
	  {
      label: '店铺名称',
      prop: 'shopName'
    },
	  {
      label: '商家名称',
      prop: 'merchantName'
    },
	  {
      label: '打印机类型',
      prop: 'printerType',
      dicUrl:'/admin/dict/type/printer_type',
    },
    {
      label: '打印机模板类型',
      prop: 'templateType',
      dicUrl:'/admin/dict/type/templateType',
    },
	  {
      label: '打印机编号',
      prop: 'printerSn',
      search: true
    },
	  {
      label: '打印机秘钥',
      prop: 'printerKey',
      hide:true
    },
	  {
      label: '打印机名',
      prop: 'printerName',
      search: true
    },
	  {
      label: '打印模板,小票样式',
      prop: 'template',
      hide:true
    },
	  {
      label: '可打印分类',
      prop: 'category',
      hide:true
    },
	  {
      label: '打印次数',
      prop: 'times',
    },
	  {
      label: '云打印状态,是否添加至飞鹅云,0 -否 1-是',
      prop: 'status',
      hide:true
    },
	  {
      label: '启用状态',
      prop: 'useable',
      dicUrl:'/admin/dict/type/scm_yes_no',
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true,
      hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea',
      hide:true
    }
  ]
}
