/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '托盘名称',
            prop: 'palletName',
            search:true
        },
        {
            label: '描述',
            prop: 'palletDesc'
        },
        {
            label: '托盘类型',
            prop: 'palletType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_pallet_type '
        },
        {
            label: '放置货品编号',
            prop: 'productSn'
        },
        {
            label: '托盘放置类型',
            prop: 'palletPutType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_pallet_put_type '
        },
        {
            label: '货位编号',
            prop: 'cellSn'
        },
        {
            label: '暂存点编号',
            prop: 'storagePlaceSn'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '存货状态',
            prop: 'storageStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_storage_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
