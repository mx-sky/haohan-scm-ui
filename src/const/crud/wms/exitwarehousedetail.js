/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '出库单编号',
            prop: 'exitWarehouseSn',
            search:true
        },
        {
            label: '出库单明细编号',
            prop: 'exitWarehouseDetailSn'
        },
        {
            label: '出库申请时间',
            prop: 'applyTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '仓库编号',
            prop: 'warehouseSn'
        },
        {
            label: '出库货品编号',
            prop: 'productSn'
        },
        {
            label: '出库货品数量',
            prop: 'productNumber'
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '出库状态',
            prop: 'exitStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_exit_status '
        },
        {
            label: '出库单类型',
            prop: 'exitType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_exit_type '
        },
        {
            label: '生产任务编号',
            prop: 'productionTaskSn'
        },
        {
            label: '汇总单编号',
            prop: 'summaryOrderId'
        },
        {
            label: '送货时间',
            prop: 'deliveryDate',
            type: "date",
            format: "yyyy-MM-dd ",
            valueFormat: "yyyy-MM-dd "
        },
        {
            label: '送货批次',
            prop: 'deliverySeq',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_delivery_seq '
        },
        {
            label: '暂存点编号(目标放置点)',
            prop: 'storagePlaceSn'
        },
        {
            label: '暂存点名称',
            prop: 'storagePlaceName'
        },
        {
            label: '验收人名称',
            prop: 'auditorName'
        },
        {
            label: '验收时间',
            prop: 'auditTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
