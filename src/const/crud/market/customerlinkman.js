/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家',
      prop: 'pmId'
    },
	  {
      label: '商家资料库',
      prop: 'customerId'
    },
	  {
      label: '姓名',
      search:true,
      prop: 'name'
    },
	  {
      label: '性别',
      prop: 'sex',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_sex'
    },
	  {
      label: '职位',
      prop: 'position'
    },
	  {
      label: '部门',
      prop: 'department'
    },
	  {
      label: '电话',
      prop: 'telephone'
    },
	  {
      label: '邮箱',
      prop: 'email'
    },
	  {
      label: '联系地址',
      prop: 'address'
    },
	  {
      label: '描述',
      prop: 'linkmanDesc'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
