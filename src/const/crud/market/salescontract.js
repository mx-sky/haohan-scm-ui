/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '合同编号',
      prop: 'salesContractSn'
    },
	  {
      label: '合同状态',
      prop: 'contractStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_contract_status'
    },
	  {
      label: '合同描述',
      prop: 'contractDesc'
    },
	  {
      label: '客户id(商家资料库id)',
      prop: 'customerId'
    },
	  {
      label: '客户联系人姓名',
      prop: 'customerLinkmanName'
    },
	  {
      label: '销售机会id',
      prop: 'salesLeadsId'
    },
	  {
      label: '合同金额',
      prop: 'contractAmount'
    },
	  {
      label: '签约时间',
      prop: 'contractTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '结束时间',
      prop: 'closedTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '条件条款',
      prop: 'conditions'
    },
	  {
      label: '合同图片地址',
      prop: 'imageUrl',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '销售分润比例',
      prop: 'salesRate'
    },
	  {
      label: '市场负责人名称',
      prop: 'directorName'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
