/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [{
      label: '平台商家',
      prop: 'pmId'
    }, {
      label: '主题内容',
      prop: 'content'
    }, {
      label: '商家资料库',
      prop: 'customerId',
      search: true
    }, {
      label: '客户联系人',
      prop: 'customerLinkmanName'
    }, {
      label: '纪念日类型',
      prop: 'anniversaryType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_anniversary_type',
      search: true
    }, {
      label: '纪念日时间',
      prop: 'anniversaryTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    }, {
      label: '日历类型',
      prop: 'calendarType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_calendar_type'

    }, {
      label: '市场负责人',
      prop: 'directorName'
    }, {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
