/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '市场任务编号',
      prop: 'marketTaskSn'
    },
	  {
      label: '发起人名称',
      prop: 'initiatorName'
    },
	  {
      label: '执行人名称',
      prop: 'transactorName'
    },
	  {
      label: '任务类别',
      prop: 'marketTaskType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_market_task_type'
    },
	  {
      label: '任务状态',
      prop: 'taskStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_task_status'
    },
	  {
      label: '售后单编号',
      prop: 'afterSalesSn'
    },
	  {
      label: '结算单编号',
      prop: 'settlementSn'
    },
	  {
      label: '客户id(商家资料库id)',
      prop: 'customerId'
    },
	  {
      label: '客户联系人id',
      prop: 'customerLinkmanId'
    },
	  {
      label: '任务内容说明',
      prop: 'taskDesc'
    },
	  {
      label: '任务执行备注',
      prop: 'actionDesc'
    },
	  {
      label: '送货日期',
      prop: 'deliveryDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '送货批次',
      prop: 'deliverySeq',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_delivery_seq'
    },
	  {
      label: '任务截止时间',
      prop: 'deadlineTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '任务操作时间',
      prop: 'actionTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
