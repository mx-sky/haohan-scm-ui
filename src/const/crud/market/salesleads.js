/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '客户id(商家资料库id)',
      prop: 'customerId'
    },
	  {
      label: '客户联系人id',
      prop: 'customerLinkmanId'
    },
	  {
      label: '销售机会名称',
      prop: 'name'
    },
	  {
      label: '销售机会状态',
      prop: 'salesStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_sales_status'
    },
	  {
      label: '客户来源',
      prop: 'customerSource'
    },
	  {
      label: '销售阶段',
      prop: 'salesStage',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_sales_stage'
    },
	  {
      label: '机会描述',
      prop: 'salesDesc'
    },
	  {
      label: '可能性',
      prop: 'salesProbability',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_sales_probability'
    },
	  {
      label: '预计成交日期',
      prop: 'estimate',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
	  {
      label: '预计成交金额',
      prop: 'estimateAmount'
    },
	  {
      label: '成交日期',
      prop: 'dealTime',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
	  {
      label: '市场负责人名称',
      prop: 'directorName'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
