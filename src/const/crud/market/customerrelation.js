/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家',
      prop: 'pmId'
    },
	  {
      label: '商家资料库',
      prop: 'customerId'
    },
	  {
      label: '员工',
      prop: 'employeeId'
    },
	  {
      label: '关系建立时间',
      prop: 'startTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '关系结束时间',
      prop: 'endTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '关系状态',
      prop: 'relationStatus',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_relation_status'
    },
	  {
      label: '采购商',
      prop: 'buyerId'
    },
	  {
      label: '销售分润比例',
      prop: 'salesRate'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
