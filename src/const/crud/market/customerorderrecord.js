/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家',
      prop: 'pmId'
    },
	  {
      label: '市场负责人',
      prop: 'directorId'
    },
	  {
      label: '订单成交日期',
      prop: 'dealTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '订单分润比例',
      prop: 'orderRate'
    },
	  {
      label: 'B客户采购单编号',
      prop: 'buyId'
    },
	  {
      label: 'B客户门店采购单',
      prop: 'shopBuyId'
    },
	  {
      label: 'C客户零售单编号',
      prop: 'goodsOrderId'
    },
	  {
      label: '成交订单类型',
      prop: 'customerOrderType',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_customer_order_type'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
