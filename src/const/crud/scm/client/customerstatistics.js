/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  // selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [
    {
      label: '订单号',
      prop: 'orderNumber',
    },
    {
      label: '采购商',
      prop: 'customerName',
      // sortable: true,
      search: true
    },
    {
      label: '结算方式',
      prop: 'billType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_bill_type',
      // sortable: true,
      search: true
    },
    {
      label: '采购贷款',
      prop: 'loan',
    },
    {
      label: '售后贷款',
      prop: 'loan',
    },
    {
      label: '售后单',
      prop: 'loan',
    },
    {
      label: '结算状态',
      prop: 'status',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_yes_no',
      // sortable: true,
      search: true
    }, {
      label:'更新时间',
      prop: "daterange",
      type: "daterange",
      startPlaceholder: '日期开始范围自定义',
      endPlaceholder: '日期结束范围自定义',
      // format: "yyyy-MM-dd",
      // valueFormat: "yyyy-MM-dd HH:mm:ss",
      // sortable: true,
      search: true
    }, {
      label: '备注信息',
      prop: 'createName'
    }
  ]
}


