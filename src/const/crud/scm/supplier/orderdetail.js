export const tableOption2 = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: true,
    addBtn: false,
    editBtn: false,
    delBtn:false,
    menu:false,
    column: [
        {
            label: '订单编号',
            prop: '',
            search:true
        },
        {
            label: '配送费用',
            prop: '',
        },
        {
            label: '应付金额',
            prop: ''
        },
        {
            label: '支付方式',
            prop: ''
        },
        {
            label: '下单时间',
            type: 'datetime',
            format: 'yyyy-MM-dd HH:mm',
            valueFormat: 'yyyy-MM-dd HH:mm:ss',
            search:true,
            prop: ''
        }

    ]
}