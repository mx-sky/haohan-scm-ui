/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '单号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 100,
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    // selection: true,
    viewBtn: false,
    // dateBtn: false,
    delBtn: false,
    addBtn: false,
    editBtn: false,
    // height: 600,
    column: [
        {
            label: '应付账单编号',
            prop: 'supplierPaymentId',
            sortable: true,
            search: true
        },
        {
            label: '来源订单编号',
            prop: 'payableSn',
            sortable: true,
            search: true
        },
        {
            label: '客户名称',
            prop: 'customerName',
            sortable: true,
        },
        {
            label: '商家名称',
            prop: 'merchantName',
            sortable: true,
        },
        {
            label: '单据金额',
            prop: 'supplierPayment'
        }, {
            label: '类型',
            prop: 'billType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_bill_type',
            sortable: true,
            search: true
        }, {
            label: '是否结算',
            prop: 'status',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_yes_no',
            sortable: true,
            search: true
        },{
            label: '审核状态',
            prop: 'reviewStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_review_status',
            sortable: true,
            search: true
        },{
            label: '订单成交时间',
            prop: 'supplyDate',
            type: 'date',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            sortable: true,
            search: true
        },
        // {
        //     label: '采购订单编号',
        //     prop: 'askOrderId',
        //     sortable: true,
        //     search: true
        // },
        // {
        //     label: '制单人',
        //     prop: 'createName'
        // }
    ]
}


