/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '请款编号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn:false,
  addBtn:false,
  editBtn: false,
  column: [
    {
      label: '请款人',
      prop: 'requestAgent',
      type: 'text',
      //sortable: true,
      search: true,
      //hide: true
    },{
      label: '请款状态',
      prop: 'requestStatus',
      type: 'select',
      //dicUrl: '/admin/dict/type/scm_purchase_order_type',
      //sortable: true,
      search: true,
    }, {
      label: '采购类型',
      prop: 'procurementType',
      type: 'select',
      // dicUrl: '/admin/dict/type/scm_purchase_order_type',
      //sortable: true,
      search: true,
      //hide: true
    }, {
      label: '请款日期',
      prop: 'requestDate'
    },{
      label: '请款金额',
      prop: 'requestAmount'
    }
  ]
}


