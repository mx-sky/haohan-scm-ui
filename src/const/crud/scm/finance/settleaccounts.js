/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '单号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn:false,
  addBtn:false,
  editBtn: false,
  column: [
    {
      label: '结算单号',
      prop: 'purchaseType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      sortable: true,
      search: true,
      hide: true
    }, {
      label: '客户名称',
      prop: 'supplyName',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      sortable: true,
      search: true,
      hide: true
    }, {
      label: '客户名称',
      prop: 'customeName'
    }, {
      label: '手机号',
      prop: 'telephone'
    }, {
      label: '应收金额',
      prop: 'receivable'
    }, {
      label: '已收金额',
      prop: 'amountReceived'
    }, {
      label: '类型',
      prop: 'type'
    },{
      label: '付款方式',
      prop: 'paymentMethod'
    },{
      label: '结算状态',
      prop: 'SettlementStatus'
    },{
      label: '创建日期',
      prop: 'date',
      type: 'date',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      search: true
    }
  ]
}


