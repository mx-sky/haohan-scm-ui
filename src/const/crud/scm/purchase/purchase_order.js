/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: false,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: false,
    viewBtn: false,
    // dateBtn: true,
    delBtn: false,
    addBtn: false,
    editBtn: false,
    column: [
        {
            label: '采购单号',
            prop: 'purchaseSn',
            search: true,
            sortable: true,
        },
        {
            label: '采购类型',
            prop: 'purchaseOrderType',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_order_type',
            sortable: true,
            search: true,
            // hide: true
        },
        // {
        //     label: '采购员',
        //     prop: 'supplyName',
        //
        // },  {
        //     label: '供应商',
        //     prop: 'supplierName'
        // },
        {
            label: '状态',
            prop: 'purchaseStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_purchase_status',

        }, {
            label: '制单人',
            prop: 'initiatorId'
        }, {
            label: '采购截至时间',
            prop: 'buyFinalTime',
            type: 'datetime',
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true
        }, {
            label: '采购总项',
            prop: 'purchaseTotal'
        }, {
            label: '已采项',
            prop: 'finish'
        },
    ]
}


