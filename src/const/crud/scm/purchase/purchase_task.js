/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 150,
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: false,
    viewBtn: false,
    // dateBtn: true,
    addBtn:false,
    delBtn:false,
    editBtn: false,
    column: [
        {
            label: '采购单号',
            prop: 'purchaseSn',
            search: true,
            sortable: true,
        },
        // {
        //     label: '采购类型',
        //     prop: 'purchaseType',
        //     type: 'select',
        //     dicUrl: '/admin/dict/type/scm_purchase_order_type',
        //     sortable: true,
        //     search: true,
        //     hide: true
        // },
        {
            label: '商品名称',
            prop: 'goodsName',
        },
        {
            label: '商品规格',
            prop: 'modelName',
        }, {
            label: '单位',
            prop: 'unit'
        },
        {
            label: '商品图片',
            prop: 'goodsImg',
            type: 'upload',
            imgWidth: 100,
            imgHeight: 50,
            listType: 'picture-img'
        }, {
            label: '市场价',
            prop: 'marketPrice'
        }, {
            label: '采购价格',
            prop: 'buyPrice'
        }, {
            label: '采购数量',
            prop: 'needBuyNum'
        }, {
            label: '实际采购数量',
            prop: 'realBuyNum'
        },{
            label: '执行人',
            prop: 'transactorName'
        },{
            label: '任务执行方式',
            prop: 'taskActionType',
            dicUrl: '/admin/dict/type/scm_task_action_type',
        },{
            label: '任务状态',
            prop: 'taskStatus',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_task_status',
            search: true,

        },{
          label: '采购状态',
          prop: 'purchaseStatus',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_purchase_status',
          search: true,

        },{
          label: '采购截止时间',
          prop: 'deadlineTime',
        },{
            label: '配送日期',
            prop: 'actionTime',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true,
            hide: true
        }
    ]
}


