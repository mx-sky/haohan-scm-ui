/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: false,
    delBtn:false,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '订单编号',
            prop: 'buyId',
            search: true
        }, {
            label: '客户名称',
            prop: 'buyerName',
        }, {
            label: '配送日期',
            prop: 'deliveryTime',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
            search: true
        }, {
            label: '配送批次',
            prop: 'buySeq',
            type:'select',
            dicUrl:'/admin/dict/type/scm_delivery_seq'
        }, {
            label: '配送方式',
            prop: 'deliveryType',
            type:'select',
            dicUrl: '/admin/dict/type/scm_delivery_type',
            search:true
        }, {
            label: '下单金额(元)',
            prop: 'genPrice'
        }, {
            label: '实际送货金额(元)',
            prop: 'totalPrice',
        },{
            label:'下单时间',
            prop:'buyTime',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd HH:mm:ss",
        },{
            label:'结算状态',
            prop:''
        },{
            label:'订单状态',
            prop:'status',
            type:'select',
            dicUrl:"/admin/dict/type/scm_buy_order_status",
            search:true
        }
    ]
}


