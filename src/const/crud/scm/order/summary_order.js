/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
import Util from '@/util/pds/utils'
let cookieUtil = new Util();

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    dialogWidth: '80%',
    labelWidth: '40%',
    selection: true,
    viewBtn: false,
    // dateBtn: false,
    delBtn:false,
    addBtn:false,
    editBtn: false,
    column: [
        {
            label: '汇总单号',
            prop: 'summaryOrderId',
        }, {
            label: '商品名称',
            prop: 'goodsName',
            search: true
        }, {
            label: '单位',
            prop: 'unit',
        }, {
            label: '订单数量',
            prop: 'orderNum'
        }, {
            label: '汇总量',
            prop: 'needBuyNum'
        }, {
            label: '库存量',
            prop: 'goodsStorage'
        }, {
            label: '待采购量',
            prop: 'purchaseNum',
        },{
            label: '配送日期',
            prop: 'deliveryTime',
            type: 'date',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd",
            searchDefault: cookieUtil.getDiffDate(1),
            searchClearable: false,
            search: true
        },{
            label: '批次',
            prop: 'buySeq',
            type: 'select',
            dicUrl: '/admin/dict/type/scm_delivery_seq',
            search: true,
            hide: true
        }
    ]
}


