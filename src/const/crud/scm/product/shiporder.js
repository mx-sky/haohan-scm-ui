/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    menuWidth: 150,
    align: 'center',
    delBtn: false,
    addBtn: false,
    editBtn: false,
    viewBtn: true,
    maxHeight: 600,
    column: [
        {
            label: '平台商家',
            prop: 'pmId',
            hide: true
        },
        {
            label: '送货单号',
            prop: 'shipId',
            search:true
        },
        {
            label: '客户订单编号',
            prop: 'buyId',
            search:true
        },
        {
            label: '采购商名称',
            prop: 'buyerId',
            hide: true
        },
        {
            label: '配送编号',
            prop: 'deliveryId',
            search:true
        },
        {
            label: '途径点',
            prop: 'pathPoint',
            hide: true
        },
        {
            label: '送达时间',
            prop: 'arriveTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '验收时间',
            prop: 'acceptTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '配送状态',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_ship_order_status'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true,
            hide: true
        },
        {
            label: '备注信息',
            prop: 'remarks',
            hide: true
        },
    ]
}
