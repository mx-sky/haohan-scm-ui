/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  // selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '盘点单号',
      prop: 'warehouseInventorySn',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      search: true

    },
    {
      label: '仓库',
      prop: 'warehouseSn',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      // sortable: true,
      search: true,
      // hide: true
    }, {
      label: '商品名称',
      prop: 'goodsName',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_purchase_order_type',
      // sortable: true,
      search: true,
      // hide: true
    }, {
      label: '单位',
      prop: 'unit',
    }, {
      label: '原有库存',
      prop: 'originalNumber'
    }, {
      label: '实际库存',
      prop: 'resultNumber'
    }, {
      label: '制单人',
      prop: 'operatorName'
    }, {
      label: '创建日期',
      prop: 'createDate',
      type: 'datetime',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      search: true
    }
  ]
}
