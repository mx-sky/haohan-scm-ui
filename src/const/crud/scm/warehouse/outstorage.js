/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  dialogWidth: '80%',
  labelWidth: '40%',
  // selection: true,
  viewBtn: false,
  // dateBtn: false,
  delBtn: false,
  addBtn: false,
  editBtn: false,
  column: [{
      label: '商品名称',
      prop: 'productName',
    },
    {
      label: '货品类型',
      prop: 'warehouse',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_product_product_type',
      // sortable: true,
      search: true,
      hide: true
    }, {
      label: '商品编码',
      prop: 'goodsModelId',
      // sortable: true,
      search: true,
    }, {
      label: '仓库',
      prop: 'supply',
    }, {
      label: '单位',
      prop: 'unit'
    },{
      label: '现有库存',
      prop: 'productNumber'
    }
  ]
}
