/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    emptyBtn: false,
    submitBtn: false,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    label: '店铺名称',
                    prop: 'name',
                    span: 8,
                },
                {
                    label: '店铺地址',
                    prop: 'address',
                    span: 8,
                },
                {
                    label: '店铺id',
                    prop: 'shopId',
                    span: 8,
                },
                {
                    label: '负责人名称',
                    prop: 'manager',
                    span: 8,
                },
                {
                    label: '店铺电话',
                    prop: 'telephone',
                    span: 8,
                },
                {
                    label: '商家名称',
                    prop: 'merchantName',
                    span: 8,
                },
                {
                    label: '启用状态',
                    prop: 'status',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_merchant_status',
                    span: 8,
                },
                {
                    label: '店铺等级',
                    prop: 'shopLevel',
                    type: 'select',
                    dicUrl: '/admin/dict/type/scm_shop_level',
                    span: 8,
                },
                {
                    label: '定位',
                    prop: 'position',
                    span: 8,
                }
            ]
        },
        {
            icon: 'el-icon-info',
            label: '描述信息',
            prop: 'group2',
            column: [
                {
                    label: '营业时间',
                    prop: 'onlineTime',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '店铺服务',
                    prop: 'shopService',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '店铺介绍',
                    prop: 'shopDesc',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '行业名称',
                    prop: 'industry',
                    span: 8,
                    maxlength: 32,
                    showWordLimit: true
                },
                {
                    label: '更新时间',
                    prop: 'updateDate',
                    span: 8,
                },
                {
                    label: '备注信息',
                    prop: 'remarks',
                    type: "textarea",
                    span: 12,
                    maxlength: 255,
                    showWordLimit: true
                }
            ]
        },
        {
            icon: 'el-icon-info',
            label: '图片信息',
            prop: 'group3',
            column: [
                {
                    label: '轮播图',
                    prop: 'photoList',
                    span: 24,
                    formslot: true
                },
                {
                    label: '店铺收款码',
                    prop: 'payCodeList',
                    span: 24,
                    formslot: true
                },
                {
                    label: '店铺二维码',
                    prop: 'qrcodeList',
                    span: 24,
                    formslot: true
                },
                {
                    label: '店铺Logo',
                    prop: 'shopLogoList',
                    span: 24,
                    formslot: true
                }
            ]
        }
    ]
}
