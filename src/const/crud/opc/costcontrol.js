/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家ID',
      prop: 'pmId',
      search: true
    },
	  {
      label: '成本名称',
      prop: 'costName'
    },
	  {
      label: '成本分类',
      prop: 'costType'
    },
	  {
      label: '计算单位',
      prop: 'countUnit'
    },
	  {
      label: '管控阀值',
      prop: 'controlLimit'
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
