/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家ID',
      prop: 'pmId',
      search: true
    },
	  {
      label: '采购商商家ID',
      prop: 'merchantId'
    },
	  {
      label: '采购商ID',
      prop: 'buyerId'
    },
	  {
      label: '商品分类ID',
      prop: 'categoryId'
    },
	  {
      label: '商品spuID',
      prop: 'goodsId'
    },
	  {
      label: '商品skuID',
      prop: 'modelId'
    },
	  {
      label: '商品分类名称',
      prop: 'categoryName'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '规格名称',
      prop: 'modelName'
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
      label: '采购价',
      prop: 'price'
    },
	  {
      label: '起始时间',
      prop: 'startDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"

    },
	  {
      label: '截止时间',
      prop: 'endDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '状态',
      prop: 'status'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
