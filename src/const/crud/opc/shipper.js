/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    "column": [
        // {
        //     "type": "input",
        //     "label": "主键",
        //     "prop": "id"
        // },
        {
            "type": "input",
            "label": "发货人姓名",
            "prop": "shipperName",
            width: 100,
            search: true,
            searchLabelWidth: 100
        }, {
            "type": "input",
            "label": "电话",
            "prop": "telephone",
            search: true
        }, {
            "type": "input",
            "label": "发货地址",
            "prop": "shipAddress",
            search: true
        }, {
            "type": "select",
            "label": "性别",
            "prop": "sex",
            dicUrl: '/admin/dict/type/scm_sex',
            search: true
        }, {
            "type": "number",
            "label": "排序值",
            "prop": "sort"
        },
        // {
        //     "type": "input",
        //     "label": "创建者",
        //     "prop": "createBy"
        // }, {
        //     "type": "input",
        //     "label": "创建时间",
        //     "prop": "createDate"
        // }, {
        //     "type": "input",
        //     "label": "更新者",
        //     "prop": "updateBy"
        // }, {
        //     "type": "input",
        //     "label": "更新时间",
        //     "prop": "updateDate"
        // },
        {
            "type": "input",
            "label": "备注信息",
            "prop": "remarks"
        },
        // {
        //     "type": "input",
        //     "label": "删除标记",
        //     "prop": "delFlag"
        // }, {
        //     "type": "input",
        //     "label": "租户id",
        //     "prop": "tenantId"
        // }
    ]
}
