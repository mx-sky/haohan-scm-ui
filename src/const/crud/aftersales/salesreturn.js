/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '售后单编号',
      prop: 'afterSalesSn'
    },
	  {
      label: '退货状态:1.待揽货2配送中3已收货',
      prop: 'salesReturnStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/sales_return_status',
      search: true
    },
	  {
      label: '申请人名称',
      prop: 'applicantName'
    },
	  {
      label: '接收人名称',
      prop: 'receiverName'
    },
	  {
      label: '揽货时间',
      prop: 'takeTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '收货时间',
      prop: 'receiveTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '接收描述',
      prop: 'receiveDesc'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
