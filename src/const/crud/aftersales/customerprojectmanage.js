/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '季度',
      prop: 'quarter'
    },
	  {
      label: '签约时间',
      prop: 'signTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '商家名称',
      prop: 'merchantName'
    },
	  {
      label: '区域',
      prop: 'area'
    },
	  {
      label: '服务产品',
      prop: 'serviceProduct',
      type: 'select',
      dicUrl: '/admin/dict/type/use_status',
      search: true
    },
	  {
      label: '服务版本',
      prop: 'serviceType'
    },
	  {
      label: '开通功能',
      prop: 'serviceList'
    },
	  {
      label: '支付方式',
      prop: 'payType',
      type: 'select',
      dicUrl: '/admin/dict/type/pay_type',
      search: true
    },
	  {
      label: '支付时间',
      prop: 'payTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '支付金额',
      prop: 'payAmount'
    },
	  {
      label: '业务专管员',
      prop: 'bizUser'
    },
	  {
      label: '运营专管员',
      prop: 'opUser'
    },
	  {
      label: '技术负责人',
      prop: 'techUser'
    },
	  {
      label: '财务核对人',
      prop: 'financeUser'
    },
	  {
      label: '项目情况',
      prop: 'projectInfo'
    },
	  {
      label: '其他说明',
      prop: 'projectDesc'
    },
	  {
      label: '项目阶段',
      prop: 'projectStep'
    },
	  {
      label: '上线状态',
      prop: 'onlineStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/use_status',
      search: true
    },
	  {
      label: '上线时间',
      prop: 'onlineTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '商家资源库ID',
      prop: 'merchantDatabase'
    },
	  {
      label: '商家ID',
      prop: 'merchant'
    },
	  {
      label: '商家应用ID',
      prop: 'merchantApp'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: "textarea"
    }
  ]
}
