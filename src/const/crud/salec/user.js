/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '用户id',
      prop: 'uid'
    },
	  {
      label: '用户账号',
      prop: 'account'
    },
	  {
      label: '用户密码',
      prop: 'pwd'
    },
	  {
      label: '用户昵称',
      prop: 'nickname'
    },
	  {
      label: '用户头像',
      prop: 'avatar',
      type: 'img',
      dataType: 'string',
      imgWidth: 60,
      imgHeight: 60,
      listType: 'picture-img'
    },
	  {
      label: '手机号码',
      prop: 'phone'
    },
	  {
      label: '添加时间',
      prop: 'addTime'
    },
	  {
      label: '添加ip',
      prop: 'addIp'
    },
	  {
      label: '最后一次登录时间',
      prop: 'lastTime'
    },
	  {
      label: '最后一次登录ip',
      prop: 'lastIp'
    },
	  {
      label: '用户余额',
      prop: 'nowMoney'
    },
	  {
      label: '用户剩余积分',
      prop: 'integral'
    },
	  {
      label: '连续签到天数',
      prop: 'signNum'
    },
	  {
      label: '1为正常，0为禁止',
      prop: 'status'
    },
	  {
      label: '等级',
      prop: 'level'
    },
	  {
      label: '推广元id',
      prop: 'spreadUid'
    },
	  {
      label: '推广员关联时间',
      prop: 'spreadTime'
    },
	  {
      label: '用户类型',
      prop: 'userType'
    },
	  {
      label: '是否为推广员',
      prop: 'isPromoter'
    },
	  {
      label: '用户购买次数',
      prop: 'payCount'
    },
	  {
      label: '下级人数',
      prop: 'spreadCount'
    },
	  {
      label: '等级清除时间为0没有清除过',
      prop: 'cleanTime'
    },
  ]
}
