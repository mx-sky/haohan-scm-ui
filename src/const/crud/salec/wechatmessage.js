/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '用户行为记录id',
      prop: 'id'
    },
	  {
      label: '用户openid',
      prop: 'openid'
    },
	  {
      label: '操作类型',
      prop: 'type'
    },
	  {
      label: '操作详细记录',
      prop: 'result'
    },
	  {
      label: '操作时间',
      prop: 'addTime'
    },
  ]
}
