/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '商品名称',
            prop: 'goodsId'
        },
        {
            label: '商品规格名称',
            prop: 'goodsModelId'
        },
        {
            label: '货品编号',
            prop: 'productSn'
        },
        {
            label: '货品名称',
            prop: 'productName',
            search:true
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '货品数量',
            prop: 'productNumber'
        },
        {
            label: '货品状态',
            prop: 'productStatus',
            type:'select',
            dicUrl:'/admin/dict/type/scm_product_status '
        },
        {
            label: '货品质量',
            prop: 'productQuality',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_product_quality '
        },
        {
            label: '货品位置状态',
            prop: 'productPlaceStatus',
            type:'select',
            dicUrl:'/admin/dict/type/scm_product_place_status '
        },
        {
            label: '货品类型',
            prop: 'productType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_product_type '
        },
        {
            label: '供应商名称',
            prop: 'supplierName'
        },
        {
            label: '入库单号',
            prop: 'enterWarehouseSn'
        },
        {
            label: '采购单明细编号',
            prop: 'purchaseDetailSn'
        },
        {
            label: '采购价格/成本价',
            prop: 'purchasePrice'
        },
        {
            label: '初始采购时间/生产日期',
            prop: 'purchaseTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '货品有效期',
            prop: 'validityTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '加工操作类型',
            prop: 'processingType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_processing_type '
        },
        {
            label: '来源货品编号',
            prop: 'sourceProductSn'
        },
        {
            label: '货品描述',
            prop: 'productDesc'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
