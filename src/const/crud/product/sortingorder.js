/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '分拣单编号',
            prop: 'sortingOrderSn'
        },
        {
            label: '客户采购单编号',
            prop: 'buyid'
        },
        {
            label: '分拣点名称',
            prop: 'sortingPlaceName',
            search:true
        },
        {
            label: '分拣截止时间',
            prop: 'deadlineTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '分拣状态',
            prop: 'sortingStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_sorting_status '
        },
        {
            label: '送货日期',
            prop: 'deliveryDate',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '送货批次',
            prop: 'deliverySeq',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_delivery_seq '
        },
        {
            label: '装车点名称',
            prop: 'deliveryPlaceName'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
