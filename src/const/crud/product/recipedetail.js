/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '配方名称',
            prop: 'recipeId',
            search: true
        },
        {
            label: '原材料使用数量',
            prop: 'useNumber'
        },
        {
            label: '使用描述',
            prop: 'useDesc'
        },
        {
            label: '原材料商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '原材料商品规格名称',
            prop: 'goodsModelName'
        },
        {
            label: '原材料商品规格单位',
            prop: 'unit'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
