/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '送货单号',
            prop: 'shipId'
        },
        {
            label: '采购商名称',
            prop: 'buyerId',
            search:true
        },
        {
            label: '配送编号',
            prop: 'deliveryId'
        },
        {
            label: '途径点',
            prop: 'pathPoint'
        },
        {
            label: '送达时间',
            prop: 'arriveTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '验收时间',
            prop: 'acceptTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '状态',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
