/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '通行证ID',
            prop: 'passportId'
        },
        {
            label: '名称',
            prop: 'name',
            search:true
        },
        {
            label: '联系电话',
            prop: 'telephone'
        },
        {
            label: '员工类型',
            prop: 'productionEmployeeType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_production_employee_type '
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
