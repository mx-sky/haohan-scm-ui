/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '配方名称',
            prop: 'recipeName'
        },
        {
            label: '启用状态',
            prop: 'useStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        {
            label: '配方描述',
            prop: 'recipeDesc'
        },
        {
            label: '成品商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '成品商品规格名称',
            prop: 'goodsModelName'
        },
        {
            label: '成品商品规格单位',
            prop: 'unit'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
