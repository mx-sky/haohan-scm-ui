/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '损耗记录编号',
            prop: 'lossRecordSn'
        },
        {
            label: '报损前货品编号',
            prop: 'productSn'
        },
        {
            label: '报损后货品编号',
            prop: 'afterProductSn'
        },
        {
            label: '损耗货品编号',
            prop: 'lossProductSn'
        },
        {
            label: '损耗类型',
            prop: 'lossType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_loss_type '
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '损耗前数量',
            prop: 'originalNumber'
        },
        {
            label: '损耗后数量',
            prop: 'resultNumber'
        },
        {
            label: '耗损数量',
            prop: 'lossNumber'
        },
        {
            label: '损耗率',
            prop: 'lossRate',
            search:true
        },
        {
            label: '损耗记录时间',
            prop: 'recordTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '采购明细编号',
            prop: 'purchaseDetailSn'
        },
        {
            label: '加工记录编号',
            prop: 'processingSn'
        },
        {
            label: '分拣明细编号',
            prop: 'sortingDetailSn'
        },
        {
            label: '送货单明细编号',
            prop: 'shipDetailSn'
        },
        {
            label: '盘点明细编号',
            prop: 'warehouseInventorySn'
        },
        {
            label: '调拨明细编号',
            prop: 'allotDetailSn'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
