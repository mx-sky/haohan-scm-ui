/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '生产任务编号',
            prop: 'productionTaskSn'
        },
        {
            label: '发起人名称',
            prop: 'initiatorName'
        },
        {
            label: '执行人名称',
            prop: 'transactorName'
        },
        {
            label: '生产任务类别',
            prop: 'productionType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_production_type '
        },
        {
            label: '任务状态',
            prop: 'taskStatus',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_task_status '
        },
        {
            label: '汇总单编号',
            prop: 'summaryOrderId'
        },
        {
            label: '需求商品规格名称',
            prop: 'goodsModelId'
        },
        {
            label: '商品名称',
            prop: 'goodsName',
            search:true
        },
        {
            label: '商品规格名称',
            prop: 'goodsModelName'
        },
        {
            label: '需求货品编号',
            prop: 'productSn'
        },
        {
            label: '货品名称',
            prop: 'productName'
        },
        {
            label: '需求数量',
            prop: 'needNumber'
        },
        {
            label: '货品单位',
            prop: 'unit'
        },
        {
            label: '任务内容说明',
            prop: 'taskContent'
        },
        {
            label: '任务执行备注',
            prop: 'actionDesc'
        },
        {
            label: '送货日期',
            prop: 'deliveryDate',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '送货批次',
            prop: 'deliverySeq',
            type:'select',
            dicUrl:'/admin/dict/type/scm_delivery_seq '
        },
        {
            label: '任务截止时间',
            prop: 'deadlineTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '任务操作时间',
            prop: 'actionTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
