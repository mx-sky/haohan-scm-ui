/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        {
            label: '平台商家',
            prop: 'pmId'
        },
        {
            label: '加工记录编号',
            prop: 'processingSn'
        },
        {
            label: '加工时间',
            prop: 'processingTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '加工操作类型',
            prop: 'processingType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_processing_type '
        },
        {
            label: '配方名称',
            prop: 'recipeName'
        },
        {
            label: '加工前货品编号',
            prop: 'originalProductSn'
        },
        {
            label: '加工后货品编号',
            prop: 'resultProductSn'
        },
        {
            label: '操作员名称',
            prop: 'operatorName',
            search: true
        },
        {
            label: '暂存点编号',
            prop: 'storagePlaceSn'
        },
        {
            label: '加工描述',
            prop: 'processingDesc'
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
    ]
}
