/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '动态编号',
      prop: 'reportSn'
    },
	  {
      label: '上报类型 0库存1销售记录',
      prop: 'reportType'
    },
	  {
      label: '客户编码',
      prop: 'customerSn'
    },
	  {
      label: '客户名称',
      prop: 'customerName'
    },
	  {
      label: '图片组',
      prop: 'photos'
    },
	  {
      label: '上报位置',
      prop: 'reportLocation'
    },
	  {
      label: '上报人',
      prop: 'reportMan'
    },
	  {
      label: '上报日期\销售日期',
      prop: 'reportTime'
    },
	  {
      label: '上报人手机号',
      prop: 'reportTel'
    },
	  {
      label: '上报状态0待确认1确认',
      prop: 'reportStatus'
    },
	  {
      label: '操作人',
      prop: 'operatorName'
    },
	  {
      label: '商品总数',
      prop: 'goodsTotalNum'
    },
	  {
      label: '商品总价格',
      prop: 'goodsTotalPrice'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
