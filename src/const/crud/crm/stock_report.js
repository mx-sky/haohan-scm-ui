'use strict';

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    addBtn:false,
    editBtn:false,
    delBtn:false,
    stripe: true,
    menu: false,
    menuAlign: 'center',
    align: 'center',
    column: [{
          label: '客户编码',
          prop: 'customerSn',
          search:true,
          minWidth: 100,
      }, {
          label: '客户',
          prop: 'customerName',
      }, {
          label: '上报人',
          prop: 'reportMan',
      }, {
          label: '库存上报日期',
          prop: 'reportDate',
          format: "yyyy-MM-dd",
          minWidth: 120,
      }, {
          label: '上报状态',
          prop: 'reportStatus',
          dicData: [{
              label: '待确认',
              value: '0',
          }, {
              label: '已确认',
              value: '1',
          }],
      }, {
          label: '上报编号',
          prop: 'reportSn',
          minWidth: 100,
      }, {
          label: '商品名称',
          prop: 'goodsName',
          search: true,
          minWidth: 100,
      }, {
          label: '规格属性',
          prop: 'modelAttr'
      }, {
          label: '商品图片',
          prop: 'goodsImg',
          width: 120,
          type:'upload',
          imgWidth:100,
          imgHeight:100,
          listType:'picture-img'
      }, {
          label: '商品规格编号',
          prop: 'modelSn',
          search: true,
          minWidth: 120,
          searchLabelWidth: 120,
      }, {
          label: '条形码',
          prop: 'modelCode',
          search: true,
      }, {
          label: '保质期',
          prop: 'expiration',
      }, {
          label: '生产日期',
          prop: 'productTime',
          format: "yyyy-MM-dd HH:mm:ss",
          valueFormat: "yyyy-MM-dd HH:mm:ss",
          minWidth: 100,
      }, {
          label: '到期日期',
          prop: 'maturityTime',
          type: 'datetime',
          format: "yyyy-MM-dd HH:mm:ss",
          valueFormat: "yyyy-MM-dd HH:mm:ss",
          minWidth: 100,
          search: true,
      }, {
          label: '批发价',
          prop: 'tradePrice',
      }, {
          label: '库存数量',
          prop: 'goodsNum',
      }, {
          label: '金额',
          prop: 'amount'
      }
    ]
}
  