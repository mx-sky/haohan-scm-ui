/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
    {
      label: '订单号',
      prop: 'salesOrderSn',
      search:true
    },
    {
      label: '客户',
      prop: 'customerName',
      search:true
    },
    {
      label: '交货日期',
      prop: 'deliveryDate',
    },
    {
      label: '总品种',
      prop: 'customerSn'
    },
    {
      label: '订单备注',
      prop: 'remarks'
    },
    {
      label: '发货仓库',
      prop: 'createBy'
    },
    {
      label: '业务员',
      prop: 'employeeName'
    },
    {
      label: '下单时间',
      prop: 'createDate'
    }
  ]
}
