/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '汇报编号',
      prop: 'reportSn',
      width: 100,
    },
	  {
      label: '上报类型 ',//0库存1销售记录
      prop: 'reportType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_data_report',
      search: true,
    },
	  {
      label: '汇报内容',
      prop: 'reportContent',
      width: 180,
      overHidden: true,
    },
	  {
      label: '汇报位置',
      prop: 'reportLocation',
      formslot: true,
      width: 180,
      overHidden: true,
    },
	  {
      label: '汇报人',
      prop: 'reportMan',
      search:true
    },
	  {
      label: '汇报日期',
      prop: 'reportTime',
      type: 'datetimerange',
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      search: true,
      sortable:true,
      width: 160,
    },
	  {
      label: '汇报人手机号',
      prop: 'reportTel',
      searchLabelWidth: 120,
      width: 120,
      search: true,
    },
	  {
      label: '状态',// 0未完成1完成
      prop: 'status',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_work_report',
      search: true,
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      type: 'datetime',
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      addDisplay: false,
      editDisplay: false,
      width: 160,
      hide: true,
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      addDisplay: false,
      editDisplay: false,
      hide: true,
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      type: 'datetime',
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      addDisplay: false,
      editDisplay: false,
      width: 160,
      hide: true,
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      hide: true,
    },
	//   {
    //   label: '删除标记',
    //   prop: 'delFlag',
    //   hide:true,
    //   addDisplay: false,
    //   editDisplay: false,
    // },
	//   {
    //   label: '租户id',
    //   prop: 'tenantId',
    //   hide:true,
    //   addDisplay: false,
    //   editDisplay: false,
    // },
  ]
}
