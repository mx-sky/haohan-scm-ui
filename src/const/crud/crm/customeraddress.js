/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '客户编号',
      prop: 'customerSn'
    },
	  {
      label: '联系人',
      prop: 'linkman'
    },
	  {
      label: '固定电话',
      prop: 'phone'
    },
	  {
      label: '送货地址',
      prop: 'address'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '是否默认',
      prop: 'defaultFlag'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
