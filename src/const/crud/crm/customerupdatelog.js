/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    menu: false,
    addBtn: false,
    column: [
        {
            label: '主键',
            prop: 'id',
            hide: true,
        },
        {
            label: '客户编号',
            prop: 'customerSn'
        },
        {
            label: '客户名称',
            prop: 'customerName',
            search: true
        },
        {
            label: '更新前',
            prop: 'updateBefore'
        },
        {
            label: '更新后',
            prop: 'updateAfter'
        },
        {
            label: '操作时间',
            prop: 'opTime',
            sortable: true,
        },
        {
            label: "操作时间",
            prop: "opTimeRange",
            format: "yyyy-MM-dd",
            type: 'date',
            valueFormat: "yyyy-MM-dd",
            search: true,
            searchRange: true,
            searchSpan: 12,
            hide: true,
        },
        {
            label: '操作人',
            prop: 'operatorName'
        },
        {
            label: '创建者',
            prop: 'createBy',
            hide: true
        },
        {
            label: '创建时间',
            prop: 'createDate',
            hide: true
        },
        {
            label: '更新者',
            prop: 'updateBy',
            hide: true
        },
        {
            label: '更新时间',
            prop: 'updateDate',
            hide: true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        {
            label: '删除标记',
            prop: 'delFlag',
            hide: true
        },
        {
            label: '租户id',
            prop: 'tenantId',
            hide: true
        },
    ]
}
