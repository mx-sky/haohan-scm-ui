/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  editBtn:false,
  addBtn:false,
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true,
    },
	  {
      label: '合同编号',
      prop: 'salesContractSn',
      width: 100,
    },
	  {
      label: '合同状态',//:1待签订2已签订3未签订
      prop: 'contractStatus',
      dicUrl: '/admin/dict/type/scm_contract_status'
    },
	  {
      label: '合同描述',
      prop: 'contractDesc',
      width: 180,
      overHidden: true,
      hide: true,
    },
	  {
      label: '客户编号',
      prop: 'customerSn',
      width: 100,
    },
	  {
      label: '客户联系人id',
      prop: 'linkmanId',
      hide:true,
    },
	  {
      label: '客户联系人姓名',
      prop: 'linkmanName',
      search:true,
      searchLabelWidth: 120,
    },
	  {
      label: '销售机会id',
      prop: 'salesLeadsId',
      hide:true
    },
	  {
      label: '合同金额',
      prop: 'contractAmount'
    },
	  {
      label: '签约时间',
      prop: 'contractTime',
      search:true,
      width: 160,
    },
	  {
      label: '结束时间',
      prop: 'closedTime',
      width: 160,
    },
	  {
      label: '条件条款',
      prop: 'conditions',
      width: 180,
      overHidden: true,
      hide: true,
    },
	  {
      label: '合同图片地址',
      prop: 'imageUrl',
      hide: true,
    },
	  {
      label: '销售分润比例',
      prop: 'salesRate'
    },
	  {
      label: '市场负责人id',
      prop: 'directorId',
      hide:true
    },
	  {
      label: '市场负责人名称',
      prop: 'directorName'
    },
	  {
      label: '创建者',
      prop: 'createBy',
      hide:true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      hide:true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      hide:true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
      hide:true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
      hide:true
    },
  ]
}
