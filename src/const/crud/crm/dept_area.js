/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    "border": true,
    "index": true,
    indexWidth: 50,
    "indexLabel": "序号",
    "stripe": true,
    "menuAlign": "center",
    "align": "center",
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    searchMenuSpan: 6,
    column: [
        {
            label: '部门id',
            prop: 'deptId',
            hide:true
        },
        {
            label: '部门名称',
            prop: 'deptName',
            minWidth:120,
            search: true
        },
        {
            "label": "区域数量",
            "prop": "areaNum",
        }
    ]
}
