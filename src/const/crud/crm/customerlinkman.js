/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '客户编号',
      prop: 'customerSn'
    },
	  {
      label: '姓名',
      prop: 'name'
    },
	  {
      label: '性别:1男2女',
      prop: 'sex'
    },
	  {
      label: '职位',
      prop: 'position'
    },
	  {
      label: '部门',
      prop: 'department'
    },
	  {
      label: '电话',
      prop: 'telephone'
    },
	  {
      label: '邮箱',
      prop: 'email'
    },
	  {
      label: '联系地址',
      prop: 'address'
    },
	  {
      label: '描述',
      prop: 'linkmanDesc'
    },
	  {
      label: '是否主联系人',
      prop: 'primaryFlag'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
