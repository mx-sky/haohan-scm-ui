/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '动态编号',
      prop: 'stateNum'
    },
	  {
      label: '来源编号',
      prop: 'sourceNum'
    },
	  {
      label: '动态类型',
      prop: 'stateType'
    },
	  {
      label: '内容',
      prop: 'content'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人',
      prop: 'reportMan'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
