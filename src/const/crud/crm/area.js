/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  maxHeight: 500,
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  defaultExpandAll:true,
  editBtn:false,
  addBtn:false,
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide:true
    },
	  {
      label: '区域编码',
      prop: 'areaSn',
      sortable: true,
    },
	  {
      label: '区域名称',
      prop: 'areaName',
      sortable: true,
      search: true,
    },
	  {
      label: '等级',
      prop: 'areaLevel',
      type: 'select',
      search: true,
      dicUrl: '/admin/dict/type/scm_level_type',
      sortable: true,
    },
	  {
      label: '归属部门',
      prop: 'belongDept',
      hide:true
    },
	  {
      label: '父ID',
      prop: 'parentId',
      hide:true
    },
	  {
      label: '父区域名称',
      prop: 'parentName',
      sortable: true,
    },
	  {
      label: '排序',
      prop: 'sort',
      sortable: true,
    },
	  {
      label: '创建者',
      prop: 'createBy',
      hide:true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      hide:true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      hide:true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
      hide:true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
      hide:true
    },
  ]
}
