/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  editBtn:false,
  detailBtn:false,
  addBtn:false,
  "column": [
	  {
      "type": "input",
      "label": "主键",
      "prop": "id",
      hide:true
    },	  {
      "type": "input",
      "label": "客户编号",
      "prop": "customerSn"
    },	  {
      "type": "input",
      "label": "客户名称",
      "prop": "customerName",
      search:true
    },	  {
      "type": "input",
      "label": "拍照员工id",
      "prop": "employeeId",
      hide:true
    },	  {
      "type": "input",
      "label": "拍照员工",
      "prop": "employeeName",
      search:true
    },	  {
      "type": "input",
      "label": "图片组编号",
      "prop": "groupNum"
    },	  {
      "type": "input",
      "label": "地址",
      "prop": "address"
    },	  {
      "type": "input",
      "label": "地址定位 (经度，纬度)",
      "prop": "position",
      hide:true
    },	  {
      "type": "input",
      "label": "创建者",
      "prop": "createBy",
      hide:true
    },	  {
      "type": "input",
      "label": "创建时间",
      "prop": "createDate",
      hide:true
    },	  {
      "type": "input",
      "label": "更新者",
      "prop": "updateBy",
      hide:true
    },	  {
      "type": "input",
      "label": "更新时间",
      "prop": "updateDate",
      hide:true
    },	  {
      "type": "input",
      "label": "备注信息",
      "prop": "remarks"
    },	  {
      "type": "input",
      "label": "删除标记",
      "prop": "delFlag",
      hide:true
    },	  {
      "type": "input",
      "label": "租户id",
      "prop": "tenantId",
      hide:true
    }  ]
}
