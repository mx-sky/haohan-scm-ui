/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexWidth: 50,
    indexLabel: '序号',
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    selection: true,
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '客户名称',
            prop: 'customerName',
            search: true
        },
        {
            label: '客户编码',
            prop: 'customerSn',
            search: true,
        },
        {
            label: '父级客户',
            prop: 'parentName',
        },
        {
            label: '排序值',
            prop: 'sort'
        },
        {
            label: '客户商家id',
            prop: 'merchantid',
            hide: true
        },
        {
            label: '客户商家',
            prop: 'merchantName',
        },
    ]
}
