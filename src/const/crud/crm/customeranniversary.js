/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn:false,
  editBtn:false,
  column: [
	  {
      label: '主键',
      prop: 'id',
      hide: true
    },
	  {
      label: '主题内容',
      prop: 'content'
    },
	  {
      label: '客户编号',
      prop: 'customerSn',
      search: true
    },
    {
      label: '客户',
      prop: 'customerName',
      search: true,
    },
	  {
      label: '客户联系人id',
      prop: 'linkmanId',
      hide: true
    },
	  {
      label: '客户联系人姓名',
      prop: 'linkmanName',
      search: true,
      searchLabelWidth: 120,
    },
	  {
      label: '纪念日类型',
      prop: 'anniversaryType',
      dicUrl:'/admin/dict/type/scm_anniversary_type',
      type: 'select',
      search: true,
      searchLabelWidth: 100,
    },
	  {
      label: '纪念日时间',
      prop: 'anniversaryTime',
      type: 'datetimerange',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd HH:mm:ss",
      searchLabelWidth: 100,
      search: true,
    },
	  {
      label: '日历类型',
      prop: 'calendarType',
      dicUrl:'/admin/dict/type/scm_calendar_type',
      type: 'select',
      search: true,
    },
	  {
      label: '市场负责人id',
      prop: 'directorId',
      hide: true
    },
	  {
      label: '市场负责人名称',
      prop: 'directorName',
      search: true,
      searchLabelWidth: 120,
    },
	  {
      label: '创建者',
      prop: 'createBy',
      hide: true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
      hide: true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
      hide: true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      hide: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      hide: true
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
      hide: true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
      hide: true
    },
  ]
}
