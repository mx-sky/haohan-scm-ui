/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
        title: '订单商品明细',
        page: false,
        border: true,
        index: true,
        indexWidth: 50,
        indexLabel: "序号",
        stripe: true,
        menu: false,
        header: false,
        align: "center",
        column: [
            {
                type: 'input',
                label: '商品名称',
                prop: 'goodsName',
            },
            {
                type: 'input',
                label: '规格',
                prop: 'goodsModelSn',
    
            },
            {
                type: 'input',
                label: '规格属性',
                prop: 'modelAttr',
    
            },
            {
                type: 'input',
                label: '单位',
                prop: 'goodsUnit',
    
            },
            {
                label: '商品图片',
                prop: 'modelUrl',
                type:'img',
                dataType: 'string',
            },
            {
                type: 'input',
                label: '条形码',
                prop: 'goodsModelCode',
    
            },
            {
                type: 'input',
                label: '数量',
                prop: 'goodsNum',
    
            },
            {
                type: 'input',
                label: '批发价',
                prop: 'tradePrice',
            },
            {
                type: "number",
                label: '保质期',
                prop: 'expiration',
                precision: 2,
                valueDefault: 0,
            },
    
        ]
    }
    