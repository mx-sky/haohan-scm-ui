/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export let tableOption = {
    border: true,
    menuAlign: 'center',
    align: 'center',
    addBtn:false,
    menu:false,
    column: [
        {
          label: '员工名称',
          prop: 'employeeName',
          search:true
        },
        {
          label: '员工类型',
          prop: 'employeeType',
          type: 'select',
          search:true,
          dicUrl: '/admin/dict/type/scm_market_employee_type', 
        },
        {
          label: '部门名称',
          prop: 'department',
          search:true
        },
        {
          label: '性别',
          prop: 'sex',
          type: 'select',
          search:true,
          dicUrl: '/admin/dict/type/scm_sex',
        },
        {
            label: '日报总数',
            prop: 'totalNum',
        },
        {
          label: '无',
          prop: 'first',
          // children: [{
          //   label: '周一（0）',
          //   prop: 'monday',
          // }]
        },{
        label: '无',
        prop: 'second',
        // children: [{
        //   label: '周二（0）',
        //   prop: 'tuesday',
        // }]
      },{
        label: '无',
        prop: 'third',
        // children: [{
        //   label: '周三（0）',
        //   prop: 'wednesday',
        // }]
      },{
        label: '无',
        prop: 'fourth',
        // children: [{
        //   label: '周四（0）',
        //   prop: 'thursday',
        // }]
      },{
        label: '无',
        prop: 'fifth',
        // children: [{
        //   label: '周五（0）',
        //   prop: 'friday',
        // }]
      },{
        label: '无',
        prop: 'sixth',
        // children: [{
        //   label: '周六（0）',
        //   prop: 'saturday',
        // }]
      },{
        label: '无',
        prop: 'seventh',
        // children: [{
        //   label: '周日（0）',
        //   prop: 'sunday',
        // }]
      },
    ]
}

/**
 * @param {*} data 数组
 */
export function getTableOption(data) {
  if(data && data.length > 0 ) {
    let reportList = data[0].reportList;
    let length = reportList.length;
    if(length > 0) {
      reportList.map((item, index) => {
        tableOption.column[index + 5]['label'] = item.reportDate;
        tableOption.column[index + 5]['prop'] = 'report-' + index;
      })
    }
  }
  return tableOption;
}

export function getTableData(data) {
  if(data && data.length > 0) {
    data = data.map(item => {
      let reportList = item.reportList;
      reportList.map((item1, index1) => {
        item['report-' + index1] = item1.reportNum;
      })
      return item;
    })
  }
  return data;
}