/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  menuAlign: 'center',
  align: 'center',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  index: true,
  indexLabel: '序号',
  column: [
    /*{
        label: '主键',
        prop: 'id'
    },*/
    {
      label: '销量上报单号',
      prop: 'reportSn',
      search: true,
      searchLabelWidth: 100,
      width: 110
    },
    {
      label: '客户编码',
      prop: 'customerSn',
      search: true,
      width: 100
    },

    {
      label: '客户',
      prop: 'customerName',
      search: true
    },
    {
      label: '上报状态',
      prop: 'reportStatus',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_data_report_status',
      search: true
    },
    {
      label: '上报人',
      prop: 'reportMan',
      search: true
    },
    {
      label: '销售日期',
      prop: 'reportDate',
      type: 'daterange',
      format: 'yyyy-MM-dd',
      valueFormat: 'yyyy-MM-dd',
      search: true,
      sortable: true,
      width: 110,
      searchSpan: 12,
      searchRange: true
    },
    // {
    //   label: '品种数',
    //   prop: 'varieties'
    // },
    {
      label: '其他金额',
      prop: 'otherAmount'
    },
    {
      label: '商品合计金额',
      prop: 'sumAmount',
      width: 110
    },
    {
      label: '总金额',
      prop: 'totalAmount'
    },
    // {
    //   label: '位置',
    //   prop: 'reportLocation'
    // },
    {
      label: '商品总数',
      prop: 'goodsTotalNum'
    },
    {
      label: '上报时间',
      prop: 'createDate',
      width: 180
    }
  ]
}
