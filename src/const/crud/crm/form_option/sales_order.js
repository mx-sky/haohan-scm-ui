/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '基础信息',
            prop: 'group1',
            column: [
                {
                    type: 'input',
                    label: '销售订单编号',
                    prop: 'salesOrderSn',
                    span: 6
                },
                {
                    type: 'input',
                    label: '客户编号',
                    prop: 'customerSn',
                    span: 6
                },
                {
                    type: 'input',
                    label: '客户名称',
                    prop: 'customerName',
                    span: 6
                },
                {
                    type: 'input',
                    label: '下单时间',
                    prop: 'orderTime',
                    span: 6
                },
                {
                    type: 'input',
                    label: '交货日期',
                    prop: 'deliveryDate',
                    span: 6
                },

                {
                    label: '销售单类型',
                    prop: 'salesType',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_sales_type',
                    span: 6
                },
                {
                    type: 'input',
                    label: '业务员名称',
                    prop: 'employeeName',
                    span: 6
                },
                {
                    type: 'input',
                    label: '收货人名称',
                    prop: 'linkmanName',
                    span: 6
                },
                {
                    type: 'input',
                    label: '收货地址',
                    prop: 'address',
                    span: 6
                },
                {
                    type: 'input',
                    label: '收货人手机号',
                    prop: 'telephone',
                    span: 6
                },
                {
                    label: '审核状态',
                    prop: 'reviewStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_review_status',
                    span: 6
                },
                {
                    type: 'input',
                    label: '货品种数',
                    prop: 'goodsNum',
                    span: 6
                },

                {
                    label: '订单备注信息',
                    prop: 'remarks',
                    type: 'input',
                }
            ]
        },
        {
            icon: 'el-icon-info',
            label: '金额相关',
            prop: 'group2',
            column: [
                {
                    type: 'input',
                    label: '商品合计金额',
                    prop: 'sumAmount',
                    span: 6
                },
                {
                    type: 'input',
                    label: '其他金额',
                    prop: 'otherAmount',
                    span: 6
                },
                {
                    type: 'input',
                    label: '总计金额',
                    prop: 'totalAmount',
                    span: 6
                },
                {
                    label: '支付状态',
                    prop: 'payStatus',
                    type: 'radio',
                    dicUrl: '/admin/dict/type/scm_yes_no',
                    span: 6
                },
            ]
        }
    ]
}
