/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const formOption = {
    labelWidth: 100,
    gutter: 50,
    group: [
        {
            icon: 'el-icon-info',
            label: '部门信息',
            prop: 'group1',
            column: [
                // {
                //     "type": "input",
                //     label: '部门id',
                //     prop: 'deptId',
                //     readonly: true,
                //     span: 8
                // },
                {
                    "type": "input",
                    label: '部门名称',
                    prop: 'deptName',
                    readonly: true,
                    span:8
                },
            ]
        }, {
            icon: 'el-icon-info',
            label: '区域信息',
            prop: 'group2',
            "column": [
                {
                    "type": "input",
                    label: '关联区域数',
                    prop: 'areaNum',
                    readonly: true,
                    span:8
                },
                {
                    "type": "input",
                    "label": "关联区域",
                    "prop": "areaSns",
                    formslot: true,
                    span:24
                }
            ]
        }]
}
