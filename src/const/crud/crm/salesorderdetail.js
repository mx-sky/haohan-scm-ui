/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '销售单编号',
      prop: 'salesOrderSn'
    },
	  {
      label: '销售单明细编号',
      prop: 'salesDetailSn'
    },
	  {
      label: '客户编号',
      prop: 'customerSn'
    },
	  {
      label: '商品销售类型:1.普通2.促销品3.赠品',
      prop: 'salesType'
    },
	  {
      label: '商品ID',
      prop: 'goodsModelId'
    },
	  {
      label: '商品图片',
      prop: 'goodsImg'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '商品规格名称',
      prop: 'modelName'
    },
	  {
      label: '采购数量',
      prop: 'goodsNum'
    },
	  {
      label: '市场价格',
      prop: 'marketPrice'
    },
	  {
      label: '成交价格',
      prop: 'dealPrice'
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
      label: '金额',
      prop: 'amount'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
