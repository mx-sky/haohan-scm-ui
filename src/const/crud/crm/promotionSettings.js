/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn:false,
  column: [
    {
      label: '促销主题',
      prop: 'name',
      search:true
    },
    {
      label: '活动开始时间',
      prop: 'startDate',
      search:true
    },
    {
      label: '活动结束时间',
      prop: 'endDate'
    },
    {
      label: '状态',
      prop: 'status_des'
    },
    {
      label: '更新时间',
      prop: 'updateDate'
    }
  ]
}
