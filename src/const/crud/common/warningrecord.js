/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '预警记录编号',
      prop: 'warningSn'
    },
	  {
      label: '使用模板编号',
      prop: 'templateSn'
    },
	  {
      label: '预警类型:各部门预警',
      prop: 'warningType'
    },
	  {
      label: '业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户',
      prop: 'departmentType'
    },
	  {
      label: '预警内容',
      prop: 'content'
    },
	  {
      label: '触发时间',
      prop: 'triggerTime'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
