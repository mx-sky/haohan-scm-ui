/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '消息ID',
      prop: 'msgTemplateId'
    },
	  {
      label: '字段名称',
      prop: 'fieldName'
    },
	  {
      label: '字段值',
      prop: 'fieldValue'
    },
	  {
      label: '字段code',
      prop: 'fileldCode'
    },
	  {
      label: '字段颜色',
      prop: 'fieldColor'
    },
	  {
      label: '是否加粗',
      prop: 'isBold'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
