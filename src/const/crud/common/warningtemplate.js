/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '模板编号',
      prop: 'templateSn'
    },
	  {
      label: '预警类型:各部门预警',
      prop: 'warningType'
    },
	  {
      label: '模板名称',
      prop: 'templateName'
    },
	  {
      label: '模板内容',
      prop: 'templateContent'
    },
	  {
      label: '模板说明',
      prop: 'templateDesc'
    },
	  {
      label: '触发条件值',
      prop: 'triggerValue'
    },
	  {
      label: '启用状态:0.未启用1.启用',
      prop: 'useStatus'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
