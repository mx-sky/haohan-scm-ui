/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家',
      prop: 'pmId'
    },
	  {
      label: '通行证ID',
      prop: 'uid'
    },
	  {
      label: '商品ID',
      prop: 'goodsId'
    },
	  {
      label: '规格ID',
      prop: 'modelId'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      text: 'textarea'
    }
  ]
}
