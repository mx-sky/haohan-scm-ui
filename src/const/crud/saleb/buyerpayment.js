/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家ID',
      prop: 'pmId',
      search: true
    },
	  {
      label: '采购编号',
      prop: 'buyId'
    },
	  {
      label: '应收来源订单编号',
      prop: 'receivableSn'
    },
	  {
      label: '采购商',
      prop: 'buyerId'
    },
	  {
      label: '采购日期',
      prop: 'buyDate',
      type: 'date',
      format: "yyyy-MM-dd",
      valueFormat: "yyyy-MM-dd"
    },
	  {
      label: '商品数量',
      prop: 'goodsNum'
    },
	  {
      label: '采购货款',
      prop: 'buyerPayment'
    },
	  {
      label: '售后货款',
      prop: 'afterSalePayment'
    },
	  {
      label: '结算状态',
      prop: 'status',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_yes_no',
      search: true,
      sortable: true
    },
	  {
      label: '运费',
      prop: 'shipFee'
    },
	  {
      label: '售后单编号',
      prop: 'serviceId'
    },
	  {
      label: '采购商货款记录编号',
      prop: 'buyerPaymentId'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
    {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
