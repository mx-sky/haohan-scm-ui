/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '汇总单号',
      prop: 'summaryBuyId'
    },
	  {
      label: '采购编号',
      prop: 'buyId'
    },
	  {
      label: '采购明细编号',
      prop: 'buyDetailSn'
    },
	  {
      label: '采购商',
      prop: 'buyerId'
    },
	  {
      label: '商品规格ID',
      prop: 'goodsModelId'
    },
	  {
      label: '商品图片',
      prop: 'goodsImg',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '商品规格',
      prop: 'goodsModel'
    },
	  {
      label: '采购数量',
      prop: 'goodsNum'
    },
	  {
      label: '市场价格',
      prop: 'marketPrice'
    },
	  {
      label: '采购价格',
      prop: 'buyPrice'
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
          label: '采购单状态',
          prop: 'status',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_buy_order_status',
          search: true,
          sortable: true
    },
	  {
      label: '零售单明细id',
      prop: 'goodsOrderDetailId'
    },
	  {
      label: '商品下单数量',
      prop: 'orderGoodsNum'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    },
	  {
      label: '是否已汇总',
      prop: 'summaryFlag',
      search: true,
      type: 'select',
      dicUrl: '/admin/dict/type/scm_yes_no'
    }
  ]
}
