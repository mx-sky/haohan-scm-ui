/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '平台商家id',
      prop: 'pmId'
    },
	  {
      label: '采购编号',
      prop: 'buyId'
    },
	  {
      label: '采购商',
      prop: 'buyerId'
    },
	  {
      label: '采购商名称',
      prop: 'buyerName'
    },
	  {
      label: '采购用户',
      prop: 'buyerUid'
    },
	  {
      label: '采购时间',
      prop: 'buyTime'
    },
	  {
      label: '送货日期',
      prop: 'deliveryTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '送货批次',
      prop: 'buySeq',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_buy_seq',
      search: true,
      sortable: true
    },
	  {
      label: '采购需求',
      prop: 'needNote'
    },
	  {
      label: '总价预估',
      prop: 'genPrice'
    },
	  {
      label: '采购总价',
      prop: 'totalPrice'
    },
	  {
      label: '联系人',
      prop: 'contact'
    },
	  {
      label: '联系电话',
      prop: 'telephone'
    },
	  {
      label: '配送地址',
      prop: 'address'
    },
	  {
      label: '采购单状态',
      prop: 'status',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_buy_order_status',
      search: true,
      sortable: true
    },
	  {
      label: '成交时间',
      prop: 'dealTime',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '运费',
      prop: 'shipFee'
    },
	  {
      label: '配送方式',
      prop: 'deliveryType',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_delivery_type',
      search: true,
      sortable: true
    },
	  {
      label: '零售单号',
      prop: 'goodsOrderId'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
