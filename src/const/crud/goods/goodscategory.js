/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '店铺ID',
      prop: 'shopId'
    },
	  {
      label: '行业名称',
      prop: 'industry'
    },
	  {
      label: '关键词',
      prop: 'keywords'
    },
	  {
      label: '父级编号',
      prop: 'parentId'
    },
	  {
      label: '所有父级编号',
      prop: 'parentIds'
    },
	  {
      label: '名称',
      prop: 'name'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '描述',
      prop: 'description'
    },
	  {
      label: 'logo地址',
      prop: 'logo',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '分类类型',
      prop: 'categoryType'
    },
	  {
      label: '分类编号',
      prop: 'categorySn'
    },
	  {
      label: '商家ID',
      prop: 'merchantId'
    },
	  {
      label: '父分类编号/即速父级ID',
      prop: 'parentSn'
    },
	  {
      label: '商品类型',
      prop: 'goodsType'
    },
	  {
      label: '商品分类通用编号',
      prop: 'generalCategorySn'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
