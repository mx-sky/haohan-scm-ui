/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '商品分类id',
      prop: 'goodsCategoryId'
    },
	  {
      label: '商品通用编号',
      prop: 'generalSn'
    },
	  {
      label: '商品描述',
      prop: 'detailDesc'
    },
	  {
      label: '缩略图地址',
      prop: 'thumbUrl',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '行业',
      prop: 'industry'
    },
	  {
      label: '品牌',
      prop: 'brand'
    },
	  {
      label: '厂家/制造商',
      prop: 'manufacturer'
    },
	  {
      label: '图片组编号',
      prop: 'photoGroupNum'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '聚合平台类型',
      prop: 'aggregationType'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
