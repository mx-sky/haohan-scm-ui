/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家id',
      prop: 'merchantId'
    },
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '赠品id',
      prop: 'giftId'
    },
	  {
      label: '赠品名称',
      prop: 'giftName'
    },
	  {
      label: '赠送规则',
      prop: 'giftRule'
    },
	  {
      label: '起始日期',
      prop: 'beginDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '结束日期',
      prop: 'endDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '赠送周期',
      prop: 'giftSchedule'
    },
	  {
      label: '赠品数量',
      prop: 'giftNum'
    },
	  {
      label: '图片地址',
      prop: 'giftUrl',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
