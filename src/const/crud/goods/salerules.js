/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商家id',
      prop: 'merchantId'
    },
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '售卖区域',
      prop: 'areaId'
    },
	  {
      label: '售卖时效',
      prop: 'saleArriveType'
    },
	  {
      label: '起售数量',
      prop: 'minSaleNum'
    },
	  {
      label: '限制购买次数',
      prop: 'limitBuyTimes'
    },
	  {
      label: '配送类型限制',
      prop: 'saleDeliveryType'
    },
	  {
      label: '起售时间',
      prop: 'beginSaleDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '售卖结束时间',
      prop: 'endSaleDate',
      type: "datetime",
      format: "yyyy-MM-dd HH:mm:ss",
      valueFormat: "yyyy-MM-dd HH:mm:ss"
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
