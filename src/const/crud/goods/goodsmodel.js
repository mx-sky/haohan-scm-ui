/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '商品id',
      prop: 'goodsId'
    },
	  {
      label: '规格价格',
      prop: 'modelPrice'
    },
	  {
      label: '规格名称',
      prop: 'modelName'
    },
	  {
      label: '规格单位',
      prop: 'modelUnit'
    },
	  {
      label: '规格库存',
      prop: 'modelStorage'
    },
	  {
      label: '规格商品图片地址',
      prop: 'modelUrl',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '扩展信息',
      prop: 'modelInfo'
    },
	  {
      label: '扫码购编码',
      prop: 'modelCode'
    },
	  {
      label: '规格组合',
      prop: 'model'
    },
	  {
      label: '即速应用规格ID',
      prop: 'itemsId'
    },
	  {
      label: '虚拟价格',
      prop: 'virtualPrice'
    },
	  {
      label: '商品规格唯一编号',
      prop: 'goodsModelSn'
    },
	  {
      label: '商品规格通用编号/公共商品库通用编号',
      prop: 'modelGeneralSn'
    },
	  {
      label: '第三方规格编号/即速商品id',
      prop: 'thirdModelSn'
    },
	  {
      label: '参考成本价',
      prop: 'costPrice'
    },
	  {
      label: '重量',
      prop: 'weight'
    },
	  {
      label: '体积',
      prop: 'volume'
    },
	  {
      label: '库存预警值',
      prop: 'stocksForewarn'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
