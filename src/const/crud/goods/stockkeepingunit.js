/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '标准商品id',
      prop: 'spuId'
    },
	  {
      label: '规格属性名id集',
      prop: 'attrNameIds'
    },
	  {
      label: '规格属性值id集',
      prop: 'attrValueIds'
    },
	  {
      label: '商品唯一编号',
      prop: 'stockGoodsSn'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '售价',
      prop: 'salePrice'
    },
	  {
      label: '库存',
      prop: 'stock'
    },
	  {
      label: '单位',
      prop: 'unit'
    },
	  {
      label: '规格详情,拼接所有属性值',
      prop: 'attrDetail'
    },
	  {
      label: '扫码条码',
      prop: 'scanCode'
    },
	  {
      label: '规格图片',
      prop: 'attrPhoto',
      type:'upload',
      imgWidth:100,
      imgHeight:50,
      listType:'picture-img'
    },
	  {
      label: '重量;单位kg',
      prop: 'weight'
    },
	  {
      label: '体积;单位立方米',
      prop: 'volume'
    },
	  {
      label: '排序',
      prop: 'sort'
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
      disabled: true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
      type: 'textarea'
    }
  ]
}
