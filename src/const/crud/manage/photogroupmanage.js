/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexWidth: 50,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    delBtn: false,
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '商家ID',
            prop: 'merchantId',
            hide: true,
        },
        {
            label: '图片组编号',
            prop: 'groupNum',
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '图片组名称',
            prop: 'groupName',
            search: true,
            searchLabelWidth: 100
        },
        {
            label: '类别标签',
            prop: 'categroyTag',
            search: true,
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate'
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
