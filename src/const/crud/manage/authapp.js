/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },
        {
            label: '应用ID',
            prop: 'appId'
        },*/
        {
            label: '应用名称',
            prop: 'appName',
            search: true
        },
        {
            label: '应用头像地址',
            prop: 'appIcon'
        },
        {
            label: '授权AppId',
            prop: 'authAppid'
        },
        {
            label: 'App秘钥',
            prop: 'appSecret'
        },
        {
            label: '访问token',
            prop: 'accessToken'
        },
        {
            label: '刷新token',
            prop: 'flushToken'
        },
        {
            label: '授权值列表',
            prop: 'authCode'
        },
        {
            label: '服务类型',
            prop: 'serviceType'
        },
        {
            label: '效验类型',
            prop: 'verifyType'
        },
        {
            label: '原appId',
            prop: 'originalAppid'
        },
        {
            label: '主体名称',
            prop: 'principalName'
        },
        {
            label: '微信号',
            prop: 'weixinId'
        },
        {
            label: '二维码地址',
            prop: 'qrcode'
        },
        {
            label: '有效期',
            prop: 'expiresin'
        },
        {
            label: '授权时间',
            prop: 'authTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
        {
            label: '授权信息',
            prop: 'authInfo'
        },
        {
            label: '状态',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
