/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '行业',
            prop: 'industry',
            search:true
        },
        {
            label: '品牌名称',
            prop: 'brand',
            search:true
        },
        {
            label: '公司logo',
            prop: 'logo'
        },
        {
            label: '品牌描述',
            prop: 'description'
        },
        {
            label: '品牌网址',
            prop: 'website'
        },
        /*{
            label: '排序',
            prop: 'sort'
        },*/
        {
            label: '状态',
            prop: 'status',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_use_status '
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
