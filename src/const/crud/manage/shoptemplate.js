/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id'
    },
	  {
      label: '行业类型',
      prop: 'industryCategory'
    },
	  {
      label: '微信模板ID',
      prop: 'wxModelId'
    },
	  {
      label: '小程序APPid',
      prop: 'appId'
    },
	  {
      label: '模板类型',
      prop: 'templateType'
    },
	  {
      label: '模板名称',
      prop: 'templateName'
    },
	  {
      label: '模板描述',
      prop: 'templateDesc'
    },
	  {
      label: '模板图片',
      prop: 'templatePic'
    },
	  {
      label: '版本号',
      prop: 'versionNo'
    },
	  {
      label: '版本描述',
      prop: 'versionDesc'
    },
	  {
      label: '上传时间',
      prop: 'uploadTime'
    },
	  {
      label: '返回信息',
      prop: 'respDesc'
    },
	  {
      label: '创建者',
      prop: 'createBy'
    },
	  {
      label: '创建时间',
      prop: 'createDate'
    },
	  {
      label: '更新者',
      prop: 'updateBy'
    },
	  {
      label: '更新时间',
      prop: 'updateDate'
    },
	  {
      label: '备注信息',
      prop: 'remarks'
    },
	  {
      label: '删除标记',
      prop: 'delFlag'
    },
	  {
      label: '租户id',
      prop: 'tenantId'
    },
  ]
}
