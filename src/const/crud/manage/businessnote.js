/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '主键',
            prop: 'id'
        },*/
        {
            label: '称呼',
            prop: 'userName',
            search:true
        },
        {
            label: '手机',
            prop: 'telephone'
        },
        {
            label: '邮箱',
            prop: 'email'
        },
        {
            label: '留言信息',
            prop: 'message'
        },
        {
            label: '商务类型',
            prop: 'bizType',
            type:'select',
            search:true,
            dicUrl:'/admin/dict/type/scm_business_type '
        },
        /*{
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },*/
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
