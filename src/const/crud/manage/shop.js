/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    indexWidth: 50,
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    addBtn: false,
    editBtn: false,
    detailBtn: false,
    delBtn: false,
    column: [
        {
            label: '店铺id',
            prop: 'shopId',
            hide: true
        },
        {
            label: '店铺名称',
            prop: 'name',
            search: true,

        },
        {
            label: '店铺地址',
            prop: 'address',
            search: true,
            minWidth: 120,
            overHidden: true
        },
        {
            label: '店铺负责人',
            prop: 'manager',
            search: true,
            searchLabelWidth: 100,
            minWidth: 100
        },
        {
            label: '店铺电话',
            prop: 'telephone',
            minWidth: 120,
            search: true,
        },
        // {
        //     label: '经度',
        //     prop: 'mapLongitude'
        // },
        // {
        //     label: '纬度',
        //     prop: 'mapLatitude'
        // },
        {
            label: '商家名称',
            prop: 'merchantName',
            search: true,
        },
        {
            label: '营业时间',
            prop: 'onlineTime',
            overHidden: true
        },
        {
            label: '店铺服务',
            prop: 'shopService',
            search: true,
            overHidden: true
        },
        // {
        //     label: '店铺模板ID',
        //     prop: 'templateId'
        // },
        {
            label: '轮播图片组编号',
            prop: 'photoGroupNum',
            hide: true
        },
        {
            label: '店铺介绍',
            prop: 'shopDesc',
            search: true,
            overHidden: true
        },
        {
            label: '启用状态',
            prop: 'status',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_merchant_status',
        },
        {
            label: '店铺收款码',
            prop: 'payCode',
            hide: true
        },
        {
            label: '店铺二维码',
            prop: 'qrcode',
            hide: true
        },
        {
            label: '店铺Logo',
            prop: 'shopLogo',
            hide: true
        },
        // {
        //     label: '店铺位置',
        //     prop: 'shopLocation'
        // },
        // {
        //     label: '店铺类型',
        //     prop: 'shopType'
        // },
        // {
        //     label: '配送距离',
        //     prop: 'deliverDistence'
        // },
        {
            label: '店铺等级',
            prop: 'shopLevel',
            type: 'select',
            search: true,
            dicUrl: '/admin/dict/type/scm_shop_level',
        },
        // {
        //     label: '是否更新即速商品',
        //     prop: 'isUpdateJisu'
        // },
        // {
        //     label: '店铺服务模式',
        //     prop: 'serviceType'
        // },
        {
            label: '行业名称',
            prop: 'industry',
            overHidden: true
        },
        // {
        //     label: '店铺分类id',
        //     prop: 'shopCategory'
        // },
        // {
        //     label: '认证类型',
        //     prop: 'authType'
        // },
        // {
        //     label: '聚合平台类型',
        //     prop: 'aggregationType'
        // },
        // {
        //     label: '交易类型',
        //     prop: 'tradeType'
        // },
        /*{
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            width: 160
        },
        {
            label: '备注信息',
            prop: 'remarks',
            overHidden: true
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
