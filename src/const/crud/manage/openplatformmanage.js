/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
    border: true,
    index: true,
    indexLabel: '序号',
    stripe: true,
    menuAlign: 'center',
    align: 'center',
    column: [
        /*{
            label: '编号',
            prop: 'id'
        },*/
        {
            label: '父级编号',
            prop: 'parentId'
        },
        {
            label: '所有父级编号',
            prop: 'parentIds'
        },
        {
            label: '行业分类',
            prop: 'industryCategory'
        },
        {
            label: '名称',
            prop: 'appName'
        },
        {
            label: '应用ID',
            prop: 'appId'
        },
        {
            label: '应用秘钥',
            prop: 'appSecrect'
        },
        {
            label: '应用类型',
            prop: 'appType',
            search:true
        },
        {
            label: '注册邮箱',
            prop: 'regEmail'
        },
        {
            label: '密码',
            prop: 'regPassword'
        },
        {
            label: '注册手机号',
            prop: 'regTelephone'
        },
        {
            label: '注册人姓名',
            prop: 'regUser',
            search:true
        },
        {
            label: '原始ID',
            prop: 'ghId'
        },
        {
            label: '服务类目',
            prop: 'serviceCategory'
        },
        {
            label: '状态',
            prop: 'status'
        },
        {
            label: '开通时间',
            prop: 'openTime',
            type: "datetime",
            format: "yyyy-MM-dd HH:mm:ss",
            valueFormat: "yyyy-MM-dd HH:mm:ss"
        },
       /* {
            label: '排序',
            prop: 'sort'
        },
        {
            label: '创建者',
            prop: 'createBy'
        },
        {
            label: '创建时间',
            prop: 'createDate'
        },
        {
            label: '更新者',
            prop: 'updateBy'
        },*/
        {
            label: '更新时间',
            prop: 'updateDate',
            disabled:true
        },
        {
            label: '备注信息',
            prop: 'remarks'
        },
        /*{
            label: '删除标记',
            prop: 'delFlag'
        },
        {
            label: '租户id',
            prop: 'tenantId'
        },*/
    ]
}
