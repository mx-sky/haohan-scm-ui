/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  column: [
	  {
      label: '主键',
      prop: 'id',
          hide:true
    },
      {
          // label: '商户订单编号，20个字符以内',
          label: '订单编号',
          prop: 'orderId',
          search: true
      },
	  {
      label: '商户ID',
      prop: 'merchantId',
          hide:true
    },
	  {
      label: '商户名称',
      prop: 'merchantName',
          search: true
    },
	  {
      label: '店铺ID',
      prop: 'shopId',
          hide:true,
          search: true
    },
	  {
      label: '店铺名称',
      prop: 'shopName',
    },
	  {
      label: '订单类型',
      prop: 'orderType',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_pay_order_type',
          search: true,
          sortable: true
    },
	  {
      label: '用户请求IP',
      prop: 'clientIp',
          hide:true
    },
	  {
      label: '支付流水号',
      prop: 'requestId'
    },
	  {
      label: '第三方平台订单id',
      prop: 'transId',
          hide:true
    },
	  {
      label: '商户编号',
      prop: 'partnerId'
    },
	  {
      label: '商品名称',
      prop: 'goodsName'
    },
	  {
      label: '订单提交日期',
      prop: 'orderTime',
      sortable: true
    },
	  {
      label: '订单金额',
      prop: 'orderAmount'
    },
	  {
      label: '授权码',
      prop: 'authCode',
          hide:true
    },
	  {
      label: '支付渠道',
      prop: 'payChannel',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_pay_channel',
          search: true,
          sortable: true
    },
	  {
      label: '支付方式',
      prop: 'payType',
          type: 'select',
          dicUrl: '/admin/dict/type/pay_type',
          search: true,
          sortable: true
    },
	  {
      label: '是否支持信用卡',
      prop: 'limitPay',
      type: 'select',
      dicUrl: '/admin/dict/type/scm_limit_pay',
      sortable: true,
      slot:true
    },
	  {
      label: '返回码',
      prop: 'respCode',
          hide:true
    },
	  {
      label: '返回码信息描述',
      prop: 'respDesc',
          hide:true
    },
	  {
      label: '返回时间',
      prop: 'respTime',
          hide:true
    },
	  {
      label: '商户订单二维码',
      prop: 'orderQrcode',
          hide:true
    },
	  {
      label: '用户标示openid',
      prop: 'openid',
          hide:true
    },
	  {
      label: '微信公众帐号ID',
      prop: 'appid',
          hide:true
    },
	  {
      label: '预支付订单号',
      prop: 'prepayId',
          hide:true
    },
	  {
      label: '签名',
      prop: 'paySign',
          hide:true
    },
	  {
      label: '时间戳',
      prop: 'timeStamp',
          hide:true
    },
	  {
      label: '随机字符串',
      prop: 'noncestr',
          hide:true
    },
	  {
      label: '支付状态',
      prop: 'payStatus',
          type: 'select',
          dicUrl: '/admin/dict/type/scm_pay_status',
          search: true,
          sortable: true
    },
	  {
      label: '订单信息',
      prop: 'orderInfo',
          hide:true
    },
	  {
      label: '渠道商支付回调地址',
      prop: 'partnerNotifyUrl',
          hide:true
    },
	  {
      label: '支付信息',
      prop: 'payInfo',
          hide:true
    },
	  {
      label: '设备编号',
      prop: 'deviceId',
          hide:true
    },
	  {
      label: '手续费',
      prop: 'fee',
          hide:true
    },
	  {
      label: '费率',
      prop: 'rate',
          hide:true
    },
	  {
      label: '创建者',
      prop: 'createBy',
          hide:true
    },
	  {
      label: '创建时间',
      prop: 'createDate',
          hide:true
    },
	  {
      label: '更新者',
      prop: 'updateBy',
          hide:true
    },
	  {
      label: '更新时间',
      prop: 'updateDate',
          hide:true
    },
	  {
      label: '备注信息',
      prop: 'remarks',
          hide:true
    },
	  {
      label: '删除标记',
      prop: 'delFlag',
          hide:true
    },
	  {
      label: '交易用户id',
      prop: 'buyerId',
          hide:true
    },
	  {
      label: '订单明细',
      prop: 'orderDetail',
          hide:true
    },
	  {
      label: '渠道编号',
      prop: 'partnerNum',
          hide:true
    },
	  {
      label: '租户id',
      prop: 'tenantId',
          hide:true
    },
  ]
}
