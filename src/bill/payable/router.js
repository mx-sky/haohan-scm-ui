import Layout from '@/page/index/'

const payableRouter = {
  path: '/payable',
  redirect: '/payable/payableList',
  component: Layout,
  meta: {title: '应付账单'},
  children: [
      {
        path: 'payableList',
        component: () => import('./index.vue'),
        name: '应付账单列表',
        meta: {title: '应付账单列表'}
      },
      {
          path: 'payableDetail',
          component: () => import('./detail.vue'),
          name: '应付账单详情',
          meta: {title: '应付账单详情'}
      },
      {
          path: 'payableReview',
          component: () => import('./review.vue'),
          name: '审核应付账单',
          meta: {title: '审核应付账单'}
      },
      {
          path: 'payableModify',
          component: () => import('./modify.vue'),
          name: '应付账单金额修改',
          meta: {title: '应付账单金额修改'}
      },
  ]
}

export default payableRouter;
