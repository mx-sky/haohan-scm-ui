import Layout from '@/page/index/'

const receivableRouter = {
  path: '/receivable',
  redirect: '/receivable/receivableList',
  component: Layout,
  meta: {title: '应收账单'},
  children: [
      {
        path: 'receivableList',
        component: () => import('./index.vue'),
        name: '应收账单列表',
        meta: {title: '应收账单列表'}
      },
      {
        path: 'receivableDetail',
        component: () => import('./detail.vue'),
        name: '应收账单详情',
        meta: {title: '应收账单详情'}
      },
      {
        path: 'receivableReview',
        component: () => import('./review.vue'),
        name: '审核应收账单',
        meta: {title: '审核应收账单'}
      },
      {
        path: 'receivableModify',
        component: () => import('./modify.vue'),
        name: '应收账单金额修改',
        meta: {title: '应收账单金额修改'}
      },
  ]
}

export default receivableRouter;
