import wares from '../../crm/goods/router' //商品管理
import dashboard from "../../crm/dashboard/router"  //数据报表
import report from '../../crm/report/router'  //数据报表
import customer from '../../crm/customer/router'  //客户数据库
import orderCon from '../../crm/order_con/router'  //订单管理
import team from '../../crm/team/router'  //员工管理
import reporting from '../../crm/reporting/router'  //消息管理
import basic from '../../crm/basic/router'  //基础信息管理
import pic from '../../crm/picturerecord/router'  //现场管理
import homeDeliveryRouter from '../../crm/home_delivery/router'  //送货服务记录
import deptAreaRouter from '../../crm/dept_area/router' // 部门负责区域

export default [
   wares,
   dashboard,
   customer,
   orderCon,
   team,
   reporting,
   basic,
   pic,
    homeDeliveryRouter,
    deptAreaRouter
]
