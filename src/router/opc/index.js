import shipRouter from "../../opc/ship_record/router"; // 发货单
import shipperRouter from "../../opc/shipper/router"; // 发货人
import photoRouter from "../../opc/photo/router"; // 图片管理
import manageRouter from "../../opc/manage/router"; //商家管理

// 运营管理模块功能
export default [
    manageRouter,
    photoRouter,
    shipRouter,
    shipperRouter,
]
