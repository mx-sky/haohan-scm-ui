import Layout from 'src/page/demo/demoRouter'
const demoRouter = {
    path: '/demo',
    redirct: '/demo/demo',
    component: Layout,
    meta: { title: 'Demo管理' },
    children:[
        {
            path: 'demo',
            component: () => import('./demo.vue'),
            name: 'demo',
            meta: {title: 'demo'}
        }
    ]
}

export default demoRouter
