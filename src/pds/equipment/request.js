import request from '@/router/axios'

export function feprinterList(obj) {
  return request({
    url: '/iot/feieprinter/page',
    method: 'post',
    data: obj
  })
}

