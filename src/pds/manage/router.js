import Layout from '@/page/index/'

const orderRouter = {
    path: '/manage',
    redirct: '/manage/merchant',
    component: Layout,
    meta: { title: '商家管理' },
    children:[
        {
            path: 'customer',
            component: () => import('./customer/index.vue'),
            name: '用户列表',
            meta: {title: '用户列表'}
        },
        {
            path: 'supplier',
            component: () => import('./supplier/index.vue'),
            name: '供应商列表',
            meta: {title: '供应商列表'}
        },
        // {
        //     path: 'buyer',
        //     component: () => import('./buyer/index.vue'),
        //     name: '采购商列表',
        //     meta: {title: '采购商列表'}
        // },{
        //     path: 'buyeradd',
        //     component: () => import('./buyer/add.vue'),
        //     name: '新增采购商',
        //     meta: {title: '新增采购商'}
        // },{
        //     path: 'buyeredit',
        //     component: () => import('./buyer/edit.vue'),
        //     name: '编辑采购商',
        //     meta: {title: '编辑采购商'}
        // },{
        //     path: 'buyerdetails',
        //     component: () => import('./buyer/details.vue'),
        //     name: '采购商详情',
        //     meta: {title: '采购商详情'}
        // }
    ]
}

export default orderRouter;
